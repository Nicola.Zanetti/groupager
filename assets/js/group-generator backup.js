//tests

//fields
var rslt = document.getElementById("result-box")
var stdnts = document.querySelectorAll(".student")
var stnm = document.querySelectorAll(".student-name")
var skill1 = document.querySelectorAll(".skill1")
var skill2 = document.querySelectorAll(".skill2")
var skill3 = document.querySelectorAll(".skill3")
var skill4 = document.querySelectorAll(".skill4")
var skill5 = document.querySelectorAll(".skill5")
var skillName = document.querySelectorAll(".skill-name")
var param = document.getElementById("switch1")


//sub-fields
var prmt = document.getElementById("parameter-box")

//others
var n_students = document.getElementById("n-students")
var n_groups = document.getElementById("n-groups")
var students_per_group = document.getElementById("students-per-group")
var group_size = document.getElementById("group-size")
var calc = document.getElementById("group-calc")

var n_groups2 = document.getElementById("n-groups2")
var students_per_group2 = document.getElementById("students-per-group2")
var calc2 = document.getElementById("group-calc2")

//buttons

var generate = document.getElementById("generate")

//arrays

var student_names = []
var group_values = []
var name_set = null
var subgroup = []
var uniqueTags = []
var student_lst = getStudentParameters()
console.log(getStudentParameters())
//variables

var subject_name = ["sujet 1", "sujet 2", "sujet 3", "sujet 4"]
var n_subjects = 4

rslt.style.display = "block"


function getStudentParameters(){
    let lst = []
    stdnts.forEach(function(item, index){
        lst[index] = {
            name : stnm[index].innerHTML,
            color : [getColor(index)],
            skills : [skillName[0].innerHTML, skillName[1].innerHTML, skillName[2].innerHTML, skillName[3].innerHTML, skillName[4].innerHTML],
            values : [skill1[index].innerHTML, skill2[index].innerHTML, skill3[index].innerHTML, skill4[index].innerHTML, skill5[index].innerHTML,],
            tags : []
        }
    }
    )
    return lst
}

function generateGroup(){
    student_lst = getStudentParameters()
    setGroupNumber();
    if (param.checked == 1){
        generateSpecific()
    } else {
        generateRandom()
    }
    dragDrop()
    console.log(charts)
    charts[0].destroy();
    getRadar();
    updateData();
}

function getColor(index){
    if(Math.max(...[skill1[index].innerHTML, 
        skill2[index].innerHTML, 
        skill3[index].innerHTML, 
        skill4[index].innerHTML, 
        skill5[index].innerHTML]) == skill1[index].innerHTML){
        return "red"
    } else if(Math.max(...[skill1[index].innerHTML, 
        skill2[index].innerHTML, 
        skill3[index].innerHTML, 
        skill4[index].innerHTML, 
        skill5[index].innerHTML]) == skill2[index].innerHTML){
        return "blue"
    } else if(Math.max(...[skill1[index].innerHTML, 
        skill2[index].innerHTML, 
        skill3[index].innerHTML, 
        skill4[index].innerHTML, 
        skill5[index].innerHTML]) == skill3[index].innerHTML){
        return "green"
    } else if(Math.max(...[skill1[index].innerHTML, 
        skill2[index].innerHTML, 
        skill3[index].innerHTML, 
        skill4[index].innerHTML, 
        skill5[index].innerHTML]) == skill4[index].innerHTML){
        return "orange"
    } else if(Math.max(...[skill1[index].innerHTML, 
        skill2[index].innerHTML, 
        skill3[index].innerHTML, 
        skill4[index].innerHTML, 
        skill5[index].innerHTML]) == skill5[index].innerHTML){
        return "purple"
    }
}

function createStatsTable(clone, i){
    const statsTableRow = document.createElement("tr")
    clone.stats.appendChild(statsTableRow)
    statsTableRow.className = "stats-row"
    for (let j = 0; j < 5; j++){
        const statsTableCol = document.createElement("td")
        statsTableCol.className = "stats-col"
        const statValue = document.createTextNode(name_set[i].values[j])
        statsTableRow.appendChild(statsTableCol)
        statsTableCol.appendChild(statValue)
    }
}

function generateRandom(){
    rslt.innerHTML = '';
    shuffleArray(student_lst)
    for(let i = 0; i < group_size.value; i++){
        let node = document.getElementById("group-blueprint");
        let clone = node.cloneNode(true);
        clone.style.display = "flex";
        clone.removeAttribute('id')
        clone.members = clone.querySelector(".members")
        clone.stats = clone.querySelector(".stats-table")
        name_set = student_lst.splice(0, group_values[i])
        for (let i = 0; i < name_set.length; i++){
            const memberName = document.createElement("div")
            const nameText = document.createTextNode(name_set[i].name)
            memberName.appendChild(nameText)
            clone.members.appendChild(memberName)
            createStatsTable(clone, i)
            if (nameText.nodeValue == name_set[i].name){
                memberName.className = "member"
                memberName.setAttribute('data-skill1', name_set[i].values[0])
                memberName.setAttribute('data-skill2', name_set[i].values[1])
                memberName.setAttribute('data-skill3', name_set[i].values[2])
                memberName.setAttribute('data-skill4', name_set[i].values[3])
                memberName.setAttribute('data-skill5', name_set[i].values[4])
                memberName.style.color = name_set[i].color[0]

            }
        }
        rslt.appendChild(clone);
    }
}

function generateSpecific(){
    rslt.innerHTML = '';
    shuffleArray(student_lst)
    createSubgroups()
    resetStudentList()
    for(let i = 0; i < group_size.value; i++){
        let node = document.getElementById("group-blueprint");
        let clone = node.cloneNode(true);
        clone.style.display = "flex";
        clone.removeAttribute('id')
        clone.members = clone.querySelector(".members")
        clone.stats = clone.querySelector(".stats-table")
        name_set = student_lst.splice(0, group_values[i])
        for (let i = 0; i < name_set.length; i++){
            const memberName = document.createElement("div")
            const nameText = document.createTextNode(name_set[i].name)
            memberName.appendChild(nameText)
            clone.members.appendChild(memberName)
            createStatsTable(clone, i)
            if (nameText.nodeValue == name_set[i].name){
                memberName.className = "member"
                memberName.setAttribute('data-skill1', name_set[i].values[0])
                memberName.setAttribute('data-skill2', name_set[i].values[1])
                memberName.setAttribute('data-skill3', name_set[i].values[2])
                memberName.setAttribute('data-skill4', name_set[i].values[3])
                memberName.setAttribute('data-skill5', name_set[i].values[4])
                memberName.style.color = name_set[i].color[0]
            }
        }
        rslt.appendChild(clone);
    }
}

function setGroupNumber(){
    if (studentNumber % group_size.value == 0){
        calc2.style.display = "none"
        n_groups.innerHTML = group_size.value
        students_per_group.innerHTML = studentNumber / group_size.value
    } else {
        n_groups.innerHTML = studentNumber % group_size.value
        n_groups2.innerHTML = group_size.value - studentNumber % group_size.value
        students_per_group.innerHTML = Math.floor(studentNumber / group_size.value) + 1
        students_per_group2.innerHTML = Math.floor(studentNumber / group_size.value)
        calc2.style.display = "inline-block"
    }
    group_values = []
    for (let i = 0; i < group_size.value.length; i++){
        for (let i = 0; i < n_groups.innerHTML; i++){
            group_values.push(students_per_group.innerHTML)
        }

        for (let i = 0; i < n_groups2.innerHTML; i++){
            group_values.push(students_per_group2.innerHTML)
        }
    }
}

function createSubgroups(){
    let allTags = []
    for (let i = 0; i < student_lst.length; i++){
        allTags.push(student_lst[i].color[0])
    }
    uniqueTags = [...new Set(allTags)]
    uniqueTags.sort();
    console.log(uniqueTags)
    for (let i = 0; i < uniqueTags.length; i++){
        subgroup[i] = student_lst.filter(checkTags.bind(null, i))
    }
    subgroup.sort()
    subgroup.reverse()
    console.log(subgroup)
}

function resetStudentList(){
    student_lst = []
    while (true){
        let check = null
        for (let i = 0; i < subgroup.length; i++){
            if (subgroup[i] != false){
                student_lst.push(subgroup[i][subgroup[i].length - 1])
                subgroup[i].pop()
            }
        }
        check = subgroup.every(function(status){
            return status == false
        })
        if (check == true){
            break
        }
    }
    console.warn(student_lst)
    for (let i = 0; i < student_lst.length; i++){
        console.log(student_lst[i])
    }
}

function checkTags(index, student){
    return student.color[0] == uniqueTags[index]
}

const shuffleArray = array => {
    for (let i = array.length - 1; i > 0; i--) {
      const j = Math.floor(Math.random() * (i + 1));
      const temp = array[i];
      array[i] = array[j];
      array[j] = temp;
    }
  }

jQuery.fn.swap = function(b){ 
    // method from: http://blog.pengoworks.com/index.cfm/2008/9/24/A-quick-and-dirty-swap-method-for-jQuery
    b = jQuery(b)[0]; 
    var a = this[0]; 
    var t = a.parentNode.insertBefore(document.createTextNode(''), a); 
    b.parentNode.insertBefore(a, b); 
    t.parentNode.insertBefore(b, t); 
    t.parentNode.removeChild(t); 
    return this; 
};

function dragDrop(){
    //Fonction Drag
    $(".member").draggable({
        revert: true,
        revertDuration: 0,
        cursor: "move",
        start: function(){
            this.id = "selected"

        },
        stop: function(){
            this.id = "unselected"
            console.warn("this")
            let arr1 = this.parentElement.children
            let arr2 = this.parentElement.nextElementSibling.children[0].children
            console.log(arr2[1])
            for (let i = 1; i < arr2.length; i++) {
                    arr2[i].children[0].innerHTML = arr1[i - 1].getAttribute("data-skill1")
                    arr2[i].children[1].innerHTML = arr1[i - 1].getAttribute("data-skill2")
                    arr2[i].children[2].innerHTML = arr1[i - 1].getAttribute("data-skill3")
                    arr2[i].children[3].innerHTML = arr1[i - 1].getAttribute("data-skill4")
                    arr2[i].children[4].innerHTML = arr1[i - 1].getAttribute("data-skill5")
            }
        }
    });
    //Fonction Drop
    $(".member").droppable({
        accept: ".member",
        hoverClass: "ui-state-active",
        drop: function(event, ui) {
            var draggable = ui.draggable, droppable = $(this)
        draggable.swap(droppable);
        let arr1 = this.parentElement.children
        let arr2 = this.parentElement.nextElementSibling.children[0].children
        for (let i = 1; i < arr2.length; i++) {
                arr2[i].children[0].innerHTML = arr1[i - 1].getAttribute("data-skill1")
                arr2[i].children[1].innerHTML = arr1[i - 1].getAttribute("data-skill2")
                arr2[i].children[2].innerHTML = arr1[i - 1].getAttribute("data-skill3")
                arr2[i].children[3].innerHTML = arr1[i - 1].getAttribute("data-skill4")
                arr2[i].children[4].innerHTML = arr1[i - 1].getAttribute("data-skill5")
        }
        updateData.call();
        }
    });

    };

//group size matters
    $("#group-size").on("change paste", function(){
        student_lst = getStudentParameters();
        if (this.value > 0){
            console.log(this.value);
        } else {
            this.value = 1;
        }
        setGroupNumber();
        generateRandom();
        dragDrop();
        console.log(charts)
        charts[0].destroy();
        getRadar();
        updateData();
    })

setGroupNumber()

if (document.getElementById('switch1').checked) {
    generateSpecific()
} else {
    generateRandom()
}
dragDrop();

