//tests

//fields
var rslt = document.getElementById("result-box")
var stdnts = document.querySelectorAll(".student")
var stnm = document.querySelectorAll(".student-name")
var skillName = document.querySelectorAll(".skill-name")
var param = document.getElementById("switch1")
var subTags = document.getElementById("switch3")
var validate = document.getElementById("validate")

//sub-fields
var prmt = document.getElementById("parameter-box")

//others
var n_students = document.getElementById("n-students").innerHTML
var n_groups = document.getElementById("n-groups")
var students_per_group = document.getElementById("students-per-group")
var group_size = document.getElementById("group-size")
var calc = document.getElementById("group-calc")

var n_groups2 = document.getElementById("n-groups2")
var students_per_group2 = document.getElementById("students-per-group2")
var calc2 = document.getElementById("group-calc2")

//buttons

var generate = document.getElementById("generate")

//arrays

var student_names = []
var group_values = []
var name_set = null
var subgroup = []
var uniqueTags = []
//variables

var subject_name = ["sujet 1", "sujet 2", "sujet 3", "sujet 4"]
var n_subjects = 4
var tagged = false

rslt.style.display = "block"


function generateGroup(){
    console.log("generate ok")
/*     console.log(param.checked)
    console.log(subTags.checked) */
    if (param.checked == 1 && subTags.checked != 1){
        console.log("specific ok")
        tagged = false
        generateSpecific()
        for (let i = 0; i < charts.length; i++){
            charts[i].destroy();
        }
        getRadar();
        updateData();
    } else if(subTags.checked == 1 && param.checked == 1){
        tagged = true
        generateTagged()
    } else {
        tagged = false
        generateRandom()
        for (let i = 0; i < charts.length; i++){
            charts[i].destroy();
        }
        getRadar();
        updateData();
    }
    dragDrop()

}


function createStatsTable(clone, i, j, group_lst){
    const statsTableRow = document.createElement("tr")
    clone.stats.appendChild(statsTableRow)
    statsTableRow.className = "stats-row"
    for (let k = 0; k < 5; k++){
        const statsTableCol = document.createElement("td")
        statsTableCol.className = "stats-col"
        const statValue = document.createTextNode(group_lst[i].member_skill[j][k])
        statsTableRow.appendChild(statsTableCol)
        statsTableCol.appendChild(statValue)
    }
}

function createTagsTable(clone, i, j, group_lst){
    const statsTableRow = document.createElement("tr")
    clone.stats.appendChild(statsTableRow)
    statsTableRow.className = "stats-row"
    for (let k = 0; k < 5; k++){
        const statsTableCol = document.createElement("td")
        statsTableCol.className = "stats-col"
        const statValue = document.createTextNode(group_lst[i].member_tag[j][k])
        statsTableRow.appendChild(statsTableCol)
        if (group_lst[i].member_tag[j][k] != undefined){
            statsTableCol.appendChild(statValue)
        } else {
            statsTableCol.innerHTML = ""
        }
    }
}

function generateRandom(){
    rslt.innerHTML = '';
    const group_lst = generateRandomGroup(group_size.value, student_lst)
    for(let i = 0; i < group_lst.length; i++){
        let node = document.getElementById("group-blueprint");
        let clone = node.cloneNode(true);
        clone.style.display = "flex";
        clone.removeAttribute('id')
        clone.members = clone.querySelector(".members")
        clone.stats = clone.querySelector(".stats-table")
        for (let j = 0; j < group_lst[i].members.length; j++){
            const memberName = document.createElement("div")
            const nameText = document.createTextNode(group_lst[i].members[j])
            memberName.appendChild(nameText)
            clone.members.appendChild(memberName)
            createStatsTable(clone, i, j, group_lst)
            if (nameText.nodeValue == group_lst[i].members[j]){
                memberName.className = "member"
                memberName.setAttribute('data-skill1', group_lst[i].member_skill[j][0])
                memberName.setAttribute('data-skill2', group_lst[i].member_skill[j][1])
                memberName.setAttribute('data-skill3', group_lst[i].member_skill[j][2])
                memberName.setAttribute('data-skill4', group_lst[i].member_skill[j][3])
                memberName.setAttribute('data-skill5', group_lst[i].member_skill[j][4])
                memberName.style.color = getColor(i, j, group_lst)

            }
        }
        rslt.appendChild(clone);
    }
}

function getColor(i, j, group_lst){
    if(Math.max(...group_lst[i].member_skill[j]) == group_lst[i].member_skill[j][0]){
        return "red"
    } else if(Math.max(...group_lst[i].member_skill[j]) == group_lst[i].member_skill[j][1]){
        return "blue"
    } else if(Math.max(...group_lst[i].member_skill[j]) == group_lst[i].member_skill[j][2]){
        return "green"
    } else if(Math.max(...group_lst[i].member_skill[j]) == group_lst[i].member_skill[j][3]){
        return "orange"
    } else if(Math.max(...group_lst[i].member_skill[j]) == group_lst[i].member_skill[j][4]){
        return "purple"
    }
}

function generateSpecific(){
    rslt.innerHTML = '';
    const group_lst = GenerateUntaggedGroup(group_size.value, student_lst)
    for(let i = 0; i < group_lst.length; i++){
        let node = document.getElementById("group-blueprint");
        let clone = node.cloneNode(true);
        clone.style.display = "flex";
        clone.removeAttribute('id')
        clone.members = clone.querySelector(".members")
        clone.stats = clone.querySelector(".stats-table")
        for (let j = 0; j < group_lst[i].members.length; j++){
            const memberName = document.createElement("div")
            const nameText = document.createTextNode(group_lst[i].members[j])
            memberName.appendChild(nameText)
            clone.members.appendChild(memberName)
            createStatsTable(clone, i, j, group_lst)
            if (nameText.nodeValue == group_lst[i].members[j]){
                memberName.className = "member"
                memberName.setAttribute('data-skill1', group_lst[i].member_skill[j][0])
                memberName.setAttribute('data-skill2', group_lst[i].member_skill[j][1])
                memberName.setAttribute('data-skill3', group_lst[i].member_skill[j][2])
                memberName.setAttribute('data-skill4', group_lst[i].member_skill[j][3])
                memberName.setAttribute('data-skill5', group_lst[i].member_skill[j][4])
                memberName.style.color = getColor(i, j, group_lst)
            }
        }
        rslt.appendChild(clone);
    }
}

function generateTagged(){
    rslt.innerHTML = '';
    const group_lst = GenerateTaggedGroup(subject_lst, student_lst)
    console.log(group_lst)
    for(let i = 0; i < group_lst.length; i++){
        let node = document.getElementById("group-blueprint");
        let clone = node.cloneNode(true);
        clone.style.display = "flex";
        clone.removeAttribute('id')
        clone.name = clone.querySelector(".group-name")
        clone.chart = clone.querySelector(".radar-chart")
        clone.members = clone.querySelector(".members")
        clone.stats = clone.querySelector(".stats-table")
        clone.name.appendChild(document.createTextNode(group_lst[i].name))
        clone.chart.innerHTML = ""
        clone.stats.innerHTML = ""
        for (let j = 0; j < group_lst[i].tags.length; j++){
            const tagCheck = document.createElement("div")
            tagCheck.className = "tag"
            tagCheck.innerHTML = group_lst[i].tags[j]
            clone.chart.appendChild(tagCheck)
            const memberTags = group_lst[i].member_tag.flat()
            if (memberTags.includes(tagCheck.innerHTML)){
                tagCheck.style.color = "green"
            } else {
                tagCheck.style.color = "red"
            }
        }
        for (let j = 0; j < group_lst[i].members.length; j++){
            const memberName = document.createElement("div")
            const nameText = document.createTextNode(group_lst[i].members[j])
            memberName.appendChild(nameText)
            clone.members.appendChild(memberName)
            createTagsTable(clone, i, j, group_lst)
            if (nameText.nodeValue == group_lst[i].members[j]){
                memberName.className = "member"
/*                 memberName.setAttribute('data-skill1', group_lst[i].member_tag[j][0])
                memberName.setAttribute('data-skill2', group_lst[i].member_tag[j][1])
                memberName.setAttribute('data-skill3', group_lst[i].member_tag[j][2])
                memberName.setAttribute('data-skill4', group_lst[i].member_tag[j][3])
                memberName.setAttribute('data-skill5', group_lst[i].member_tag[j][4]) */
                //memberName.style.color = getColor(i, j, group_lst)
            }
        }
        rslt.appendChild(clone);
    }
}

function setGroupNumber(){
    if (studentNumber % group_size.value == 0){
        calc2.style.display = "none"
        n_groups.innerHTML = group_size.value
        students_per_group.innerHTML = studentNumber / group_size.value
    } else {
        n_groups.innerHTML = studentNumber % group_size.value
        n_groups2.innerHTML = group_size.value - studentNumber % group_size.value
        students_per_group.innerHTML = Math.floor(studentNumber / group_size.value) + 1
        students_per_group2.innerHTML = Math.floor(studentNumber / group_size.value)
        calc2.style.display = "inline-block"
    }
    group_values = []
    for (let i = 0; i < group_size.value.length; i++){
        for (let i = 0; i < n_groups.innerHTML; i++){
            group_values.push(students_per_group.innerHTML)
        }

        for (let i = 0; i < n_groups2.innerHTML; i++){
            group_values.push(students_per_group2.innerHTML)
        }
    }
}

function createSubgroups(){
    let allTags = []
    for (let i = 0; i < student_lst.length; i++){
        allTags.push(student_lst[i].color[0])
    }
    uniqueTags = [...new Set(allTags)]
    uniqueTags.sort();
    console.log(uniqueTags)
    for (let i = 0; i < uniqueTags.length; i++){
        subgroup[i] = student_lst.filter(checkTags.bind(null, i))
    }
    subgroup.sort()
    subgroup.reverse()
    console.log(subgroup)
}

function checkTags(index, student){
    return student.color[0] == uniqueTags[index]
}

function validateGroups(){
    const classKey = window.location.href.split("class=")[1]
    const validatedGroupLst = []
    const groupNodes = [].slice.call(document.querySelectorAll(".members"))
    groupNodes.splice(0, 1)
    const groupMembers = new Array(...defineEmptyArray(groupNodes.length))
    for (let i = 0; i < groupNodes.length; i++){
        for (let item of groupNodes[i].children){
            groupMembers[i].push(item.innerHTML)
        }
    }
    console.log(groupMembers)
    for (let i = 0; i < groupMembers.length; i++){
        validatedGroupLst.push({
            name: "Group " + (i + 1),
            members: groupMembers[i],
            class: classKey
        })
    }
    const request = new XMLHttpRequest()
    request.open("POST", "includes/json-handler.php", true)
    request.setRequestHeader("Content-Type", "application/json")
    request.send(JSON.stringify(validatedGroupLst))
}


const shuffleArray = array => {
    for (let i = array.length - 1; i > 0; i--) {
      const j = Math.floor(Math.random() * (i + 1));
      const temp = array[i];
      array[i] = array[j];
      array[j] = temp;
    }
  }

validate.addEventListener("click", validateGroups)

jQuery.fn.swap = function(b){ 
    // method from: http://blog.pengoworks.com/index.cfm/2008/9/24/A-quick-and-dirty-swap-method-for-jQuery
    b = jQuery(b)[0]; 
    var a = this[0]; 
    var t = a.parentNode.insertBefore(document.createTextNode(''), a); 
    b.parentNode.insertBefore(a, b); 
    t.parentNode.insertBefore(b, t); 
    t.parentNode.removeChild(t); 
    return this; 
};

function dragDrop(){
    //Fonction Drag
    $(".member").draggable({
        revert: true,
        revertDuration: 0,
        cursor: "move",
        start: function(){
            this.id = "selected"

        },
        stop: function(){
            this.id = "unselected"
            let arr1 = this.parentElement.children
            let arr2 = this.parentElement.nextElementSibling.children[0].children
            for (let i = 1; i < arr2.length; i++) {
                    arr2[i].children[0].innerHTML = arr1[i - 1].getAttribute("data-skill1")
                    arr2[i].children[1].innerHTML = arr1[i - 1].getAttribute("data-skill2")
                    arr2[i].children[2].innerHTML = arr1[i - 1].getAttribute("data-skill3")
                    arr2[i].children[3].innerHTML = arr1[i - 1].getAttribute("data-skill4")
                    arr2[i].children[4].innerHTML = arr1[i - 1].getAttribute("data-skill5")
            }
        }
    });
    //Fonction Drop
    $(".member").droppable({
        accept: ".member",
        hoverClass: "ui-state-active",
        drop: function(event, ui) {
            var draggable = ui.draggable, droppable = $(this)
            draggable.swap(droppable);
            let arr1 = this.parentElement.children
            let arr2 = this.parentElement.nextElementSibling.children[0].children
            for (let i = 1; i < arr2.length; i++) {
                    arr2[i].children[0].innerHTML = arr1[i - 1].getAttribute("data-skill1")
                    arr2[i].children[1].innerHTML = arr1[i - 1].getAttribute("data-skill2")
                    arr2[i].children[2].innerHTML = arr1[i - 1].getAttribute("data-skill3")
                    arr2[i].children[3].innerHTML = arr1[i - 1].getAttribute("data-skill4")
                    arr2[i].children[4].innerHTML = arr1[i - 1].getAttribute("data-skill5")
            }
            if (tagged != true){
                updateData.call();
            }
        }
    });

    };

    $("#group-size").on("change paste", function(){
        if (this.value > 0){

        } else {
            this.value = 1;
        }
        setGroupNumber();
        generateRandom();
        dragDrop();
        for (i = 0; i< charts.length; i++){
            charts[i].destroy();
        }
        getRadar();
        updateData();
    })

setGroupNumber()

generateRandom()

dragDrop();    


