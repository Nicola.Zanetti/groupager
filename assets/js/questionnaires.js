var logo = document.getElementById("logo");
var questionnaireNameInput = document.getElementById("questionnaire-name-input")
var classNameField = document.getElementById("class-name-field")
var headerBtn = document.getElementById("header-button");
var qloader = document.getElementById("qloader");
var qCreation = document.getElementById("qcreation");
var qAddBtn = document.getElementById("q-add-btn");
var qCloseBtn = document.getElementById("q-close-btn")
var qSelectBtn = document.getElementById("q-select-btn")
var questionList = document.getElementById("question-list");
var multipleChoiceBtn = document.getElementById("multiple-choice-btn")
var evaluationBtn = document.getElementById("evaluation-btn")
var freeTextBtn = document.getElementById("free-text-btn")
var questionList = document.getElementById("question-list")

//---------------VARIABLE DECLARATIONS---------------//
var qname = null
var parameters = []

//---------------INIT---------------//

qCreation.style.display = "none";
qSelectBtn.style.display = "none";

//---------------EVENT LISTENERS---------------//

logo.addEventListener("click", function(){
    window.location.href = "index.html";
})

headerBtn.addEventListener("click", function(){
    qloader.style.display = "none"
    headerBtn. style.display = "none"
    qCreation.style.display = "flex"

})

qAddBtn.addEventListener("click", function(){
    qAddBtn.style.display = "none"
    qSelectBtn.style.display = "flex";
})


qCloseBtn.addEventListener("click", function(){
    qAddBtn.style.display = "flex"
    qSelectBtn.style.display = "none";
})

multipleChoiceBtn.addEventListener("click", function(){
    qAddBtn.style.display = "flex"
    qSelectBtn.style.display = "none";
    let node = document.getElementById("multiple-choice");
    let inactiveNode = document.getElementById("multiple-choice-inactive");
    let clone = node.cloneNode(true);
    let inactiveClone = inactiveNode.cloneNode(true);
    clone.style.display = "block";
    inactiveClone.removeAttribute('id')
    clone.removeAttribute('id')
    questionList.appendChild(clone);
    questionList.appendChild(inactiveClone);
    clone.addEventListener('keydown', (evt) => {
        if (evt.keyCode === 13) {
            evt.preventDefault();
        }
    })
    addChoiceBtn = document.querySelectorAll(".add-choice")
    addChoiceBtn.forEach(function(item, index){
        if (index != 0 && addChoiceBtn[index].className != "add-choice active"){
            addChoiceBtn[index].className = "add-choice active"
            item.addEventListener("click", function(){
                let node = document.getElementsByClassName("choice-parent");
                let clone = node[index].cloneNode(true);
                clone.style.display = "flex";
                choiceLister = document.getElementsByClassName("choice-lister")
                choiceLister[index].appendChild(clone);
                clone.addEventListener('keydown', (evt) => {
                    if (evt.keyCode === 13) {
                        evt.preventDefault();
                    }
                });
                choices = document.getElementsByClassName('choice')
            })
        }
    })

    okBtn = document.querySelectorAll(".ok-btn")
    activeBox = document.getElementsByClassName("question-box")
    inactiveBox = document.querySelectorAll(".question-box-inactive")
    console.log(activeBox)
    console.log(inactiveBox)
    okBtn.forEach(function(item, index){
        if (index > 2 && okBtn[index].className != "ok-btn active"){
            okBtn[index].className = "ok-btn active"
            item.addEventListener("click", function(){
                this.parentElement.style.display = "none";
                console.log(index)
                console.log(inactiveBox)
                inactiveBox[index].style.display = "block";
            })
        }
    })
})

evaluationBtn.addEventListener("click", function(){
    qAddBtn.style.display = "flex"
    qSelectBtn.style.display = "none";
    var node = document.getElementById("evaluation");
    var inactiveNode = document.getElementById("evaluation-inactive");
    var clone = node.cloneNode(true);
    var inactiveClone = inactiveNode.cloneNode(true);
    clone.style.display = "block";
    questionList.appendChild(clone);
    questionList.appendChild(inactiveClone);
    clone.addEventListener('keydown', (evt) => {
        if (evt.keyCode === 13) {
            evt.preventDefault();
        }
    });
})

freeTextBtn.addEventListener("click", function(){
    qAddBtn.style.display = "flex"
    qSelectBtn.style.display = "none";
    var node = document.getElementById("free-answer");
    var inactiveNode = document.getElementById("free-inactive");
    var clone = node.cloneNode(true);
    var inactiveClone = inactiveNode.cloneNode(true);
    clone.style.display = "block";
    questionList.appendChild(clone);
    questionList.appendChild(inactiveClone);
    clone.addEventListener('keydown', (evt) => {
        if (evt.keyCode === 13) {
            evt.preventDefault();
        }
    });
})

//---------------FUNCTIONS---------------//

function addChoice(){
    
}

function load_questionnaires(){
    for (i = 0; i < Object.keys(savedQuestionnaires).length; i++){
        var newDiv = document.createElement("div");
        newDiv.innerHTML = "say hello to the new div";
        qloader.appendChild(newDiv); 
        var setClass = document.createAttribute("class");
        setClass.value = "saved-questionnaire";
        newDiv.setAttributeNode(setClass);
    }
}

function generate_question_form(){
    console.log("question form ok")
    var newQ = document.createElement("div");
    newQ.innerHTML = "say hello to the new question";
    questionList.appendChild(newQ); 
    var setClass = document.createAttribute("class");
    setClass.value = "saved-questionnaire";
    newDiv.setAttributeNode(setClass);
}

function initNameField(){
    classNameField.innerHTML = localStorage.getItem("className")
}

function getName(val) {
    qname = val
    setParameters()
}

function setParameters(){
    parameters[0] = qname
    alert(parameters)
}

initNameField()
load_questionnaires()