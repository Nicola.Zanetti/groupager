var elements = document.getElementsByClassName('testChart');
var grouparray = document.getElementsByClassName('members');
var memberarray = document.getElementsByClassName('member');
var startingLabels = [
    'Wri',
    'It',
    'Com',
    'Vis',
    'Rel'
  ]
var startingData = [0, 0, 0, 0, 0]

let charts = []

const data = {
    labels: startingLabels,
    datasets: [{
      label: 'Compétences',
      data: startingData,
      fill: true,
      backgroundColor: 'rgba(255, 99, 132, 0.2)',
      borderColor: 'rgb(255, 99, 132)',
      pointBackgroundColor: 'rgb(255, 99, 132)',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgb(255, 99, 132)',
      color: 'rgb(255, 99, 132)'
    }, {
        label: 'Acceptable',
        data: [60, 60, 60, 60, 60],
        fill: false,
        backgroundColor: 'rgba(54, 162, 235, 0.2)',
        borderColor: 'rgb(0, 0, 0)',
        pointBackgroundColor: 'rgba(0, 0, 0, 0)',
        pointBorderColor: 'rgba(0, 0, 0, 0)',
        pointHoverBackgroundColor: 'rgba(0, 0, 0, 0)',
        pointHoverBorderColor: 'rgba(0, 0, 0, 0)'
      }]
  };
        
  const config = {
    type: 'radar',
    data: data,
    options: {
        scales: {
            r: {
                suggestedMin: 0,
                suggestedMax: 100
            }
        },
      elements: {
        line: {
          borderWidth: 3
        }
      },
      plugins: {
        legend: {
          display: false
        }
      }
    },
  };

function getRadar(){
    for(i = 0; i < elements.length; i++){
        charts[i] = new Chart(
            elements[i],
            config
        );
    }
}

function addData(chart, label, data) {
    chart.data.labels.push(label);
    chart.data.datasets.forEach((dataset) => {
        dataset.data.push(data);
    });
    chart.update();
}

function removeData(chart) {
    chart.data.labels.pop();
    chart.data.datasets.forEach((dataset) => {
        dataset.data.pop();
    });
    chart.update();
}

function replaceData(chart, label, val, cycle) {
    if (val >= chart.data.datasets[0].data[startingLabels.indexOf(label)]){
        chart.data.datasets[0].data[startingLabels.indexOf(label)] = Number(val);
    }
    chart.update();
}

function updateData(){
    for(let i = 1; i < grouparray.length; i++){
        for(let j = 0; j < grouparray[i].children.length; j++){
            replaceData(charts[i], "Wri", grouparray[i].children[j].getAttribute('data-skill1'))
            replaceData(charts[i], "It", grouparray[i].children[j].getAttribute('data-skill2'))
            replaceData(charts[i], "Com", grouparray[i].children[j].getAttribute('data-skill3'))
            replaceData(charts[i], "Vis", grouparray[i].children[j].getAttribute('data-skill4'))
            replaceData(charts[i], "Rel", grouparray[i].children[j].getAttribute('data-skill5'))
        }
        charts[i].data.datasets[0].data = [0, 0, 0, 0, 0];
    }
}

getRadar();
updateData();