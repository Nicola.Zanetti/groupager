function generateRandomGroup(n, l) {
    shuffleArray(l)
    console.info("Random ok")
    const group_list = []
    let group_size = Math.floor(l.length / n) 
    let remaining = l.length % n
    generateEmptyValueGroups(group_list, n, group_size, remaining)
    insertStudentsRandom(group_list, l)
    return group_list
}

function insertStudentsRandom(g, l) {
    for (let i = 0; i < l.length; i++){
        for (let j = 0; j < g.length; j++){
            if(!g.some(item => item.members.includes(l[i].name)) && g[j].members.length < g[j].size){
                g[j].members.push(l[i].name)
                g[j].member_skill.push(l[i].skills)
            }
        }
    }
}

function getGroupSize(d, r, i) {
    console.info("group size ok")
    if (r != 0 && i + 1 <= r){
        return d + 1
    } else {
        return d
    }
}

function generateEmptyValueGroups(g, n, d, r){
    console.log("generate empty ok random")
    for (let i = 0; i < n; i++){
        g.push({
            name: "Group " + (i + 1),
            tags: ["writing", "it", "communication", "visual", "relations"],
            size: getGroupSize(d, r, i),
            members: [],
            member_skill: [],
            tag_check: [false, false, false, false]
        })
    }
}

function getLongestArray(obj) {
    console.info("get longest ok")
    let max = 0
    obj.forEach(function(item){
        if (item.skills.length > max){
            max = item.skills.length
        }
    })
    return max
}

function defineEmptyArray(n){
    console.info("define empty ok")
    const arr = []
    for (let i = 0; i < n; i++) {
        arr.push([])
    }
    return arr
}

function splitByValues(l){
    console.info("split ok")
    const list_by_tag = new Array(...defineEmptyArray(6))
    for (let i = 0; i < l.length; i++) {
        for (let j = 0; j <= 6; j++){
            if (l[i].skills.filter(value => value > 50).length == j) {
                list_by_tag[j].push({
                    name: l[i].name,
                    tags: l[i].skills
                })
            }
        }
    }
    return list_by_tag
}

