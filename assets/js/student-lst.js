var student_lst = [
    {
        name : "Giorgio",
        age : 0,
        sex : "M",
        tags : ["red"]
    },
    {
        name : "Alberta",
        age : 0,
        sex : "F",
        tags : ["blue"]
    },
    {
        name : "Giovanni",
        age : 0,
        sex : "M",
        tags : ["yellow"]
    },
    {
        name : "Paola",
        age : 0,
        sex : "F",
        tags : ["green"]
    },
    {
        name : "Alfredo",
        age : 0,
        sex : "M",
        tags : ["red"]
    },
    {
        name : "Stefania",
        age : 0,
        sex : "F",
        tags : ["blue"]
    },
    {
        name : "Carlo",
        age : 0,
        sex : "M",
        tags : ["yellow"]
    },
    {
        name : "Elisabetta",
        age : 0,
        sex : "F",
        tags : ["green"]
    },
    {
        name : "Giulio",
        age : 0,
        sex : "M",
        tags : ["red"]
    },
    {
        name : "Gianna",
        age : 0,
        sex : "F",
        tags : ["blue"]
    },
    {
        name : "Federico",
        age : 0,
        sex : "M",
        tags : ["yellow"]
    },
    {
        name : "Maria",
        age : 0,
        sex : "F",
        tags : ["green"]
    },
    {
        name : "Mario",
        age : 0,
        sex : "M",
        tags : ["red"]
    },
    {
        name : "Rosa",
        age : 0,
        sex : "F",
        tags : ["blue"]
    },
    {
        name : "Luigi",
        age : 0,
        sex : "M",
        tags : ["yellow"]
    },
    {
        name : "Viola",
        age : 0,
        sex : "F",
        tags : ["green"]
    },
    {
        name : "Luca",
        age : 0,
        sex : "M",
        tags : ["red"]
    },
    {
        name : "Lorenzo",
        age : 0,
        sex : "M",
        tags : ["blue"]
    },
    {
        name : "Veronica",
        age : 0,
        sex : "F",
        tags : ["yellow"]
    }
]