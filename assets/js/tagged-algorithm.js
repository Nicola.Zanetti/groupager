function GenerateTaggedGroup(s, l) {
    shuffleArray(l)
    console.info("Generate ok")
    const group_list = []
    let group_size = Math.floor(l.length / s.length) 
    let remaining = l.length % s.length
    let splitted = splitByTag(l)
    generateEmptyGroups(group_list, s, group_size, remaining)
    insertStudentsWithTags(group_list, splitted)
    insertRemainingTaggedStudents(group_list, splitted)
    return group_list
}

function insertRemainingTaggedStudents(g, t) {
    for (let i = 0; i < t.flat().length; i++){
        for (let j = 0; j < g.length; j++){
            if(!g.some(item => item.members.includes(t.flat()[i].name)) && g[j].members.length < g[j].size){
                g[j].members.push(t.flat()[i].name)
                g[j].member_tag.push(t.flat()[i].tags)
            }
        }
    }
}

function insertStudentsWithTags(g, t){
    console.info("insert students | ok")
    for (let i = 0; i < t.length; i++){
        let max_repeat = 0
        let old_repeat = 0
        let count = 0
        let x= 0
        console.warn(i)
        while (x < 1000 && count < 10){
            x++
            let repeat = 0
            for (let j = 0; j < g.length; j++){
                for (let k = 0; k < t[i].length; k++){
                    if(!g.some(item => item.members.includes(t[i][k].name)) && i > 0 && !g[j].member_tag.flat().some(item => t[i][k].tags.includes(item)) && g[j].tags.some(item => t[i][k].tags.includes(item)) && g[j].members.length < g[j].size){
                        g[j].members.push(t[i][k].name)
                        g[j].member_tag.push(t[i][k].tags)
                        break
                    } else if (!g.some(item => item.members.includes(t[i][k].name))){
                        repeat++
                        if (repeat > max_repeat) {
                            old_repeat = max_repeat
                            max_repeat = repeat
                            count = 0
                        }
                        if (old_repeat == max_repeat - 1 && count < 10) {
                            console.info(repeat + " " + old_repeat + " " + max_repeat)
                            count++
                        }
                    }
                }
            }
        }
    }
}

function generateEmptyGroups(g, s, d, r){
    for (let i = 0; i < s.length; i++){
        g.push({
            name: s[i].name,
            tags: s[i].tags,
            size: getGroupSize(d, r, i),
            members: [],
            member_tag: []
        })
    }
}

function getLongestArray(obj) {
    console.info("get longest ok")
    let max = 0
    obj.forEach(function(item){
        if (item.tags.length > max){
            max = item.tags.length
        }
    })
    return max
}

function defineEmptyArray(n){
    console.info("define empty ok")
    const arr = []
    for (let i = 0; i < n; i++) {
        arr.push([])
    }
    return arr
}

function splitByTag(l){
    console.info("split ok")
    const list_by_tag = new Array(...defineEmptyArray(getLongestArray(l) + 1))
    for (let i = 0; i < l.length; i++) {
        for (let j = 0; j <= getLongestArray(l); j++){
            if (l[i].tags.length == j) {
                list_by_tag[j].push({
                    name: l[i].name,
                    tags: l[i].tags
                })
            }
        }
    }
    return list_by_tag
}