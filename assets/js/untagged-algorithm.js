function GenerateUntaggedGroup(n, l) {
    shuffleArray(student_lst)
    console.log("Untagged ok")
    const group_list = []
    let group_size = Math.floor(l.length / n) 
    let remaining = l.length % n
    let splitted = splitByValues(l)
    console.log(splitted)
    generateEmptyValueGroups(group_list, n, group_size, remaining)
    insertStudents(group_list, splitted)
    console.log(group_list)
    insertRemainingStudents(group_list, splitted)
    return group_list
}

function insertRemainingStudents(g, t) {
    for (let i = 0; i < t.flat().length; i++){
        for (let j = 0; j < g.length; j++){
            if(!g.some(item => item.members.includes(t.flat()[i].name)) && g[j].members.length < g[j].size){
                g[j].members.push(t.flat()[i].name)
                g[j].member_skill.push(t.flat()[i].skills)
            }
        }
    }
}

function insertStudents(g, t){
    for (let i = 0; i < t.length; i++){
        g = g.reverse()
        let count = 0
        let max_repeat = 0
        let old_repeat = 0
        let x = 0
        while (x < 1000 && count < 10){
            x++
            let repeat = 0
            for (let j = 0; j < g.length; j++){
                group:
                for (let k = 0; k < t[i].length; k++){
                    for (let l = 0; l < 5; l++){
                        if(!g.some(item => item.members.includes(t[i][k].name))
                            && i > 0
                            && g[j].tag_check[l] != true
                            && g[j].members.length < g[j].size
                            && t[i][k].skills[l] > 60){
                                g[j].members.push(t[i][k].name)
                                g[j].member_skill.push(t[i][k].skills)
                                t[i][k].skills.forEach(function(item, index){
                                    if (item > 60){
                                        g[j].tag_check[index] = true
                                    } 
                                })
                                break group
                        } else if (!g.some(item => item.members.includes(t[i][k].name && l == 0))){
                            repeat++
                            if (repeat > max_repeat) {
                                old_repeat = max_repeat
                                max_repeat = repeat
                                count = 0
                            }
                            if (old_repeat == max_repeat - 1 && count < 10) {
                                console.info(repeat + " " + old_repeat + " " + max_repeat)
                                count++
                            }
                        }
                    }
                }
            }
        }
    }
}

function splitByValues(l){
    const list_by_tag = new Array(...defineEmptyArray(6))
    for (let i = 0; i < l.length; i++) {
        for (let j = 0; j <= 6; j++){
            if (l[i].skills.filter(value => value > 60).length == j) {
                list_by_tag[j].push({
                    name: l[i].name,
                    skills: l[i].skills
                })
            }
        }
    }
    for (let i = list_by_tag.length - 1; i >= 0; i--) {
        console.log(i)
        console.log(list_by_tag[i] == false)
        if (list_by_tag[i] == false){
            console.log("pop")
            list_by_tag.pop()
        } else {
            i = -1
        }
    }
    console.log(list_by_tag)
    return list_by_tag
}