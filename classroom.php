<?php
    include_once 'header.php';
    include_once 'includes/dbh.inc.php';
    include_once 'includes/functions.inc.php';
    ob_start();
    $n_students = getStudentList($conn);
    ob_end_clean();
    $subjectGroup = getSubjectName($conn);
?>

    <main>
        <section style="display: flex;">
            <div class="generator-options">
                <div class="option-title">
                    Options
                </div>
                <div class="option">
                    <div style="flex-basis: 130px;">Paramètres</div>
                    <label class="switch">
                        <input id="switch1" name="parameters" type="checkbox" onclick="generateGroup()">
                        <span class="slider"></span>
                    </label>
                </div>
<!--                 <div class="option">
                    <div style="flex-basis: 130px;">Questionnaire</div>
                    <label class="switch">
                        <input id="switch2" type="checkbox" onclick="reloadPage()">
                        <span class="slider"></span>
                    </label>
                </div> -->
                <div class="option">
                    <div style="flex-basis: 130px;">Sujets</div>
                    <label class="switch">
                        <input id="switch3" type="checkbox" onclick="checkSubjects()">
                        <span class="slider"></span>
                    </label>
                </div>
                <div id="validate" class="validate-btn">
                    Valider groupes
                </div>
            </div>
            <div class="generator-right">
            <div class="title-field">
                <?php 
                    echo "<h2 class='title'><span style='color: orange;'>".getClassName($conn)."</span></h2>";
                    echo "<a class='change' href='welcome.php?t=teacher'>Changer classe</a>";
                    echo "<a class='change' href='subject.php?class=".$_GET['class']."'>Gérer Sujets</a>";
                    echo "<p style='color: orange;'>Sujet: ".$subjectGroup."</p>";
                ?>
            </div>
                <div id="size" class="student-formulary">
                    <div style="padding-bottom: 9px;">
                        <label for="group-size">N. groupes</label>
                        <input id="group-size" type="number" name="group-size" value="2" min="1">
                    </div>
                    <span id="group-calc">Dans votre classe il y a <span id="n-students" style="color: orange"><?php echo $n_students ?></span> étudiants,
                        <span id="n-groups" style="color: orange">0</span> groupes de <span id="students-per-group" style="color: orange">0</span> personnes
                        <span id="group-calc2">et <span id="n-groups2" style="color: orange">0</span> groupes de <span id="students-per-group2" style="color: orange">0</span> personnes.</span>
                    </span>
                </div>
                <div id="group-blueprint" class="group-box" style="display: none; flex-wrap: wrap; justify-content: flex-start;">
                    <div class="group-name"></div>
                    <div class="radar-chart">
                        <canvas class="testChart"></canvas>
                    </div>
                    <div class="members"></div>
                    <div class="member-stats">
                        <table class="stats-table">
                            <tr class="stats-row">
                                <td class="stats-col">Writing</td>
                                <td class="stats-col">It</td>
                                <td class="stats-col">Comm.</td>
                                <td class="stats-col">Visual</td>
                                <td class="stats-col">Relations</td>
                            </tr>
                        </table>
                    </div>
                </div>
                <div class="generator-content">
                    <div style="font-size: 2rem; padding: 10px 0; text-align: center;">Student list</div>
                    <div class="student-list">
                        <table class="student-table">
                            <?php 
                                getStudentList($conn);
                            ?>
                        </table>
                    </div>
                    <div id="result-box" class="result-box">

                    </div>
                </div>
            </div>
        </section>
    </main>

<?php
    include_once 'footer.php';
?>
    <script>
        console.info("script start")
        var skill1 = document.querySelectorAll(".skill1")
        var skill2 = document.querySelectorAll(".skill2")
        var skill3 = document.querySelectorAll(".skill3")
        var skill4 = document.querySelectorAll(".skill4")
        var skill5 = document.querySelectorAll(".skill5")
        var studentNumber = "<?php echo $n_students; ?>"
        var hasSubject = "<?php echo subjectExists($conn); ?>"

        function checkSubjects() {
            if (hasSubject == false) {
                setTimeout(function(){
                    document.getElementById("switch3").checked = false;
                }, 200)
                alert("Vous n'avez pas crée de sujets, veuillez en créer au moins un.");
            } else if (hasSubject == true){
                generateGroup()
            }
        }

        function getStudentParameters(){
            const student_names = "<?php echo getStudentsNames($conn, $_GET['class'])?>".split(", ")
            const student_skills = "<?php echo getStudentsSkills($conn, $_GET['class']) ?>".split(", ")
            const student_tags = "<?php echo implode(", ", flatten(getStudentsTags($conn, $_GET['class'])))?>".split(", ")
            student_skills.forEach(function(item, index){
                student_skills[index] = item.split(' ')
            })
            student_tags.forEach(function(item, index){
                student_tags[index] = item.split(' ')
            })
            const student_lst = []
            student_names.forEach(function(item, index){
                student_lst[index] = {
                    name : student_names[index],
                    skills : student_skills[index],
                    tags : student_tags[index]
                }
            })
            console.info("get student parameters | ok")
            return student_lst
        }

        function getSubjectList(){
            const subject_names = "<?php echo implode(" ", flatten(getSubjectNames($conn, $subjectGroup)))?>".split(" ")
            const subject_tags = "<?php echo implode(", ", flatten(getSubjectTags($conn, $subjectGroup)))?>".split(", ")
            const subject_lst = []
            subject_names.forEach(function(item, index){
                subject_lst[index] = {
                    name : subject_names[index],
                    tags : subject_tags[index].split(" ")
                }
            })
            console.info("get subject parameters | ok")
            return subject_lst
        }

        var student_lst = getStudentParameters()
        var subject_lst = getSubjectList()
    </script>
    <script src="https://cdn.jsdelivr.net/npm/chart.js"></script>
    <script src="assets/lib/jquery-ui/external/jquery/jquery.js"></script>
    <script src="assets/lib/jquery-ui/jquery-ui.min.js"></script>
    <script src="assets/js/common.js"></script>
    <script src="assets/js/random-algorithm.js"></script>
    <script src="assets/js/untagged-algorithm.js"></script>
    <script src="assets/js/tagged-algorithm.js"></script>
    <script src="assets/js/group-generator.js"></script>
    <script src="assets/js/radar-charts.js"></script>
</body>

<?php
/* TEST AREA */
/* echo getStudentsSkills($conn, $_GET['class'], sendQuery(2)) */
$str_json = file_get_contents('php://input');
echo $str_json;
?>
</html>