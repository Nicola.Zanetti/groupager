<?php
    session_start();
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Groupager</title>
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Josefin+Sans&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="assets/css/reset.css"/>
    <link rel="stylesheet" href="assets/css/style.css"/>
    <link rel="stylesheet" href="assets/css/generator.css"/>
    <?php
    $url = 'http://' . $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI'];
    if (isset($_GET['t'])) {
        if ($_GET['t'] == "student") {
            echo "<style> 
                .header {
                    background-color: #00ACF8;
                }
            </style>";
        } else if ($_GET['t'] == "teacher") {
            echo "<style> 
                .header {
                    background-color: #DD75FF;
                }
            </style>";
        }
    } else if (strpos($url,'classroom') !== false || strpos($url,'subject') !== false) {
        echo "<style> 
            .header {
                background-color: #DD75FF;
            }
            </style>";
        } else if (strpos($url,'student') !== false){
            echo "<style> 
                .header {
                    background-color: #00ACF8;
                }
            </style>";
        } else {
            echo "invalid URL";
        }
    ?>
</head>
<body>
    <header class="header">
        <div class="wrapper hd">
            <div id="logo">
                <h1><a href="index.php">GROUPAGER</a></h1>
            </div>
            <?php
                if (isset($_SESSION["user_email"])) {
                    echo "<a class='hd-login' href='includes/logout.inc.php'>Logout</a>";
                } else {
                    echo "<a class='hd-login' href='login.php'>Login</a>";
                }
            ?>
        </div>
    </header>