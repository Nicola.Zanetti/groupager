<?php

session_start();

if (isset($_GET['t'], $_POST['submit'])) {
    $className = $_POST["class"];
    $status = $_GET["t"];
    if ($status = "student" && isset($_SESSION['user_id'])){
        $studentId = $_SESSION['user_id'];
    }

    require_once 'dbh.inc.php';
    require_once 'functions.inc.php';

    if (emptyInputClass($className) !== false) {
        header("location: ../welcome.php?t=".$_GET['t']."&error=empty");
        exit();
    }
    
    if ($_GET['t'] == "teacher") {
        createClass($conn, $className, generateCode($conn), generateKey($conn), $_SESSION['user_id'], $status);
    } else if ($_GET['t'] == "student") {
        validateClass($conn, $className, $studentId);
    }
    

} else {
    header("location: ../login.php");
    exit();
}