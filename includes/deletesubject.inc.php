<?php

session_start();

if (isset($_POST['submit'])) {

    require_once 'dbh.inc.php';
    require_once 'functions.inc.php';
    
    $group = $_GET['id'];
    $subject = $_POST['delete']*2;
    deleteSubject($conn, $group, $subject);  

} else {
    header("location: ../login.php");
    exit();
}