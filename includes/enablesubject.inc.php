<?php

session_start();

if (isset($_POST['submit'])) {
    require_once 'dbh.inc.php';
    require_once 'functions.inc.php';
    if ($_POST['submit'] == "Enabled"){
        resetEnabled($conn, $_GET['class']);
    } else {
        $id = $_POST['enable'];
        resetEnabled($conn, $_GET['class']);
        enableSubject($conn, $id);  
    }
} else {
    //header("location: ../login.php");
    exit();
}