<?php

function emptyInputSignup($first, $last, $email, $pwd, $pwdRepeat){
    $result = false;
    if (empty($first) || empty($last) || empty($email) || empty($pwd) || empty($pwdRepeat)) {
        $result = true;
    } else {
        $result = false;
    }
    return $result;
}

function invalidName($first) {
    $result = false;
    if(!preg_match("/^[a-zA-ZÀ-ÿ]*$/", $first)) {
        $result = true;
    } else {
        $result = false;
    }
    return $result;
}

function invalidSurname($last) {
    $result = false;
    if(!preg_match("/^[a-zA-ZÀ-ÿ]*$/", $last)) {
        $result = true;
    } else {
        $result = false;
    }
    return $result;
}

function invalidEmail($email) {
    $result = false;
    if(!filter_var($email, FILTER_VALIDATE_EMAIL)) {
        $result = true;
    } else {
        $result = false;
    }
    return $result;
}

function pwdMatch($pwd, $pwdRepeat) {
    $result = false;
    if($pwd !== $pwdRepeat) {
        $result = true;
    } else {
        $result = false;
    }
    return $result;
}

function emailExists($conn, $email, $status) {
    $sql = "SELECT * FROM users WHERE user_email = ? AND user_status = ?;";
    $stmt = mysqli_stmt_init($conn);
    if (!mysqli_stmt_prepare($stmt, $sql)) {
        header("location: ../signup.php?t=".$_GET['t']."&error=stmtfail");
        exit();
    }

    mysqli_stmt_bind_param($stmt, "ss", $email, $status);
    mysqli_stmt_execute($stmt);

    $resultData = mysqli_stmt_get_result($stmt);

    if ($row = mysqli_fetch_assoc($resultData)) {
        mysqli_stmt_close($stmt);
        return $row;
    } else {
        $result = false;
        mysqli_stmt_close($stmt);
        return $result;
    }
}

function createUser($conn, $first, $last, $email, $pwd, $status) {
    $sql = "INSERT INTO users (user_first, user_last, user_email, user_pwd, user_status) 
            VALUES (?, ?, ?, ?, ?);";
    $stmt = mysqli_stmt_init($conn);
    if (!mysqli_stmt_prepare($stmt, $sql)) {
        header("location: ../signup.php?error=stmtfail");
        exit();
    }

    $hashedPwd = password_hash($pwd, PASSWORD_DEFAULT); 

    mysqli_stmt_bind_param($stmt, "sssss", $first, $last, $email, $hashedPwd, $status);
    mysqli_stmt_execute($stmt);
    mysqli_stmt_close($stmt);
    header("location: ../login.php?t=".$_GET['t']);
    exit();
}

function emptyInputLogin($email, $pwd){
    $result = false;
    if (empty($email) || empty($pwd)) {
        $result = true;
    } else {
        $result = false;
    }
    return $result;
}

function loginUser($conn, $email, $pwd, $status){
    $emailExists = emailExists($conn, $email, $status);

    if ($emailExists === false) {
        header("location: ../login.php?t=".$_GET['t']."&error=wrongemail");
        exit();
    }

    $pwdHashed = $emailExists["user_pwd"];
    $checkPwd = password_verify($pwd, $pwdHashed);

    if ($checkPwd === false) {
        header("location: ../login.php?t=".$_GET['t']."&error=wronglogin");
    } else if ($checkPwd === true) {
        session_start();
        $_SESSION["user_id"] = $emailExists["user_id"];
        $_SESSION["user_email"] = $emailExists["user_email"];
        $_SESSION["user_status"] = $emailExists["user_status"];
        if (isset($_GET['t'])){
            if ($_GET['t'] == "teacher") {
                header("location: ../welcome.php?t=teacher");
                exit();
            } else if ($_GET['t'] == "student") {
                header("location: ../welcome.php?t=student");
                exit();
            }
        }
    }
}

function emptyInputClass($className){
    $result = false;
    if (empty($className)) {
        $result = true;
    } else {
        $result = false;
    }
    return $result;
}

function createClass($conn, $class_name, $class_code, $class_key, $class_ownerId, $status) {
    $sql = "INSERT INTO classes (class_name, class_code, class_key, class_ownerId) 
            VALUES (?, ?, ?, ?);";
    $stmt = mysqli_stmt_init($conn);
    if (!mysqli_stmt_prepare($stmt, $sql)) {
        header("location: ../welcome.php?t=".$_GET['t']."&error=stmtfail");
        exit();
    }

    mysqli_stmt_bind_param($stmt, "ssss", $class_name, $class_code, $class_key, $class_ownerId);
    mysqli_stmt_execute($stmt);
    mysqli_stmt_close($stmt);
    header("location: ../welcome.php?t=".$status);
    exit();
}

function checkKeys($conn, $randStr) {
    $sql = "SELECT * FROM classes";
    $result = mysqli_query($conn, $sql);

    while ($row = mysqli_fetch_assoc($result)) {
        if ($row['class_key'] == $randStr) {
            $keyExists = true;
            break;
        } else {
            $keyExists = false;
        }
    }
    return $keyExists;
}

function generateKey($conn) {
    $keyLength = 16;
    $str = "1234567890abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ()$=";
    $randStr = substr(str_shuffle($str), 0, $keyLength);

    $checkKey = checkKeys($conn, $randStr);

    while ($checkKey == true) {
        $randStr = substr(str_shuffle($str), 0, $keyLength);
        $checkKey = checkKeys($conn, $randStr);
    }

    return $randStr;
}

function checkCodes($conn, $randStr) {
    $sql = "SELECT * FROM classes";
    $result = mysqli_query($conn, $sql);

    while ($row = mysqli_fetch_assoc($result)) {
        if ($row['class_code'] == $randStr) {
            $keyExists = true;
            break;
        } else {
            $keyExists = false;
        }
    }
    return $keyExists;
}

function generateCode($conn) {
    $keyLength = 1;
    $str = "1234567890ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    $randStr =  substr(str_shuffle($str), 0, $keyLength).
                substr(str_shuffle($str), 0, $keyLength).
                substr(str_shuffle($str), 0, $keyLength).
                substr(str_shuffle($str), 0, $keyLength).
                substr(str_shuffle($str), 0, $keyLength).
                substr(str_shuffle($str), 0, $keyLength);

    $checkKey = checkKeys($conn, $randStr);

    while ($checkKey == true) {
        $randStr = substr(str_shuffle($str), 0, $keyLength);
        $checkKey = checkKeys($conn, $randStr);
    }

    return $randStr;
}

function getClassesTeacher($conn) {
    $userId = $_SESSION['user_id'];
    $sql = "SELECT class_name, class_code, class_ownerId, class_key
            FROM classes
            WHERE  classes.class_ownerId = ?;";
    
    $stmt = mysqli_stmt_init($conn);
    if (!mysqli_stmt_prepare($stmt, $sql)) {
        header("location: ../signup.php?error=stmtfail");
        exit();
    }
    mysqli_stmt_bind_param($stmt, "s", $userId);
    mysqli_stmt_execute($stmt);

    $result = mysqli_stmt_get_result($stmt);
    $resultCheck = mysqli_num_rows($result);

    if ($resultCheck == mysqli_num_rows($result)) {
        while ($row = mysqli_fetch_assoc($result)) {
            echo "<tr>";
            echo "<td class='classlink'><a href='classroom.php?class=".$row['class_key']."'>".$row['class_name']."</a></td>";
            echo "<td>".$row['class_code']."</td>";
            echo "</tr>";
        }
        mysqli_stmt_close($stmt);
    }
}

function getClassesStudent($conn) {
    $studentId = $_SESSION['user_id'];
    $sql = "SELECT class_name, classes.class_code, class_registration.class_code, class_ownerId, class_key, class_registration.student_id, user_first, user_last, class_ownerId, user_id 
            FROM classes, class_registration, users 
            WHERE class_registration.class_code = classes.class_code 
            AND class_registration.student_id = ? 
            AND class_ownerId = user_id; ";
    
    $stmt = mysqli_stmt_init($conn);
    if (!mysqli_stmt_prepare($stmt, $sql)) {
        header("location: ../signup.php?error=stmtfail");
        exit();
    }
    mysqli_stmt_bind_param($stmt, "s", $studentId);
    mysqli_stmt_execute($stmt);

    $result = mysqli_stmt_get_result($stmt);
    $resultCheck = mysqli_num_rows($result);

    if ($resultCheck = mysqli_num_rows($result)) {
        while ($row = mysqli_fetch_assoc($result)) {
            echo "<tr>";
            echo "<td class='classlink'><a href='student.php?class=".$row['class_key']."'>".$row['class_name']."</a></td>";
            echo "<td>".$row['class_code']."</td>";
            echo "<td>".$row['user_first']." ".$row['user_last']."</td>";
            echo "</tr>";
        }
        mysqli_stmt_close($stmt);
    }
}

function classExists($conn, $class) {
    $sql = "SELECT * FROM classes WHERE class_code = ?;";
    $stmt = mysqli_stmt_init($conn);
    if (!mysqli_stmt_prepare($stmt, $sql)) {
        header("location: ../welcome.php?t=student&error=stmtfail");
        exit();
    }

    mysqli_stmt_bind_param($stmt, "s", $class);
    mysqli_stmt_execute($stmt);

    $resultData = mysqli_stmt_get_result($stmt);

    if ($row = mysqli_fetch_assoc($resultData)) {
        mysqli_stmt_close($stmt);
        return $row;
    } else {
        $result = false;
        mysqli_stmt_close($stmt);
        return $result;
    }
}

function checkRegistered($conn, $class, $studentId) {
    $sql = "SELECT * FROM class_registration WHERE class_code = ? AND student_id = ?;";
    $stmt = mysqli_stmt_init($conn);
    if (!mysqli_stmt_prepare($stmt, $sql)) {
        header("location: ../welcome.php?t=student&error=stmtfail");
        exit();
    }

    mysqli_stmt_bind_param($stmt, "ss", $class, $studentId);
    mysqli_stmt_execute($stmt);

    $resultData = mysqli_stmt_get_result($stmt);

    if ($row = mysqli_fetch_assoc($resultData)) {
        mysqli_stmt_close($stmt);
        return $row;
    } else {
        $result = false;
        mysqli_stmt_close($stmt);
        return $result;
    }
}

function registerStudent($conn, $class, $studentId) {
    $sql = "INSERT INTO class_registration (class_code, student_id) 
            VALUES (?, ?);";
    $stmt = mysqli_stmt_init($conn);
    if (!mysqli_stmt_prepare($stmt, $sql)) {
        header("location: ../welcome.php?t=student&error=stmtfail");
        exit();
    } 

    mysqli_stmt_bind_param($stmt, "ss", $class, $studentId);
    mysqli_stmt_execute($stmt);
    mysqli_stmt_close($stmt);
}

function validateClass($conn, $class, $studentId){

    $classExists = classExists($conn, $class);

    if ($classExists === false) {
        header("location: ../welcome.php?t=student&error=invalidcode");
        exit();
    } else {
        $checkRegistered = checkRegistered($conn, $class, $studentId);
        if ($checkRegistered === false){
            registerStudent($conn, $class, $studentId);
        }
        header("location: ../student.php?class=".$classExists['class_key']);
        exit();
    }
}

function getClassName($conn) {
    $classKey = $_GET['class'];
    $sql = "SELECT class_name 
            FROM classes
            WHERE class_key = ?;";
    
    $stmt = mysqli_stmt_init($conn);
    if (!mysqli_stmt_prepare($stmt, $sql)) {
        header("location: ../signup.php?error=stmtfail");
        exit();
    }
    mysqli_stmt_bind_param($stmt, "s", $classKey);
    mysqli_stmt_execute($stmt);

    $result = mysqli_stmt_get_result($stmt);

    if ($row = mysqli_fetch_assoc($result)) {
        return $row['class_name'];
    }
}

function sendSkillsParameters($conn, $studentId, $skillNames, $skills, $classKey) {
    $sql = "INSERT INTO skillsets (student_id, skill1_name, skill1_value, 
                                skill2_name, skill2_value,
                                skill3_name, skill3_value, 
                                skill4_name, skill4_value, 
                                skill5_name, skill5_value) 
    VALUES (?, ?, ? ,? ,? ,? ,? ,? ,? ,? ,?);";
    $stmt = mysqli_stmt_init($conn);
    if (!mysqli_stmt_prepare($stmt, $sql)) {
        header("location: ../welcome.php?t=student&error=stmtfail");
        exit();
    } 
    mysqli_stmt_bind_param($stmt, "sssssssssss", $studentId, $skillNames[0], $skills[0], 
                                                            $skillNames[1], $skills[1], 
                                                            $skillNames[2], $skills[2], 
                                                            $skillNames[3], $skills[3], 
                                                            $skillNames[4], $skills[4]);
    mysqli_stmt_execute($stmt);

    mysqli_stmt_close($stmt);
    header("location: ../student.php?class=".$classKey);
}

function checkParameters($conn, $studentId) {
    $sql = "SELECT * FROM skillsets WHERE student_id = ?;";
    $stmt = mysqli_stmt_init($conn);
    if (!mysqli_stmt_prepare($stmt, $sql)) {
        header("location: ../welcome.php?t=student&error=stmtfail");
        exit();
    }
    mysqli_stmt_bind_param($stmt, "s", $studentId);
    mysqli_stmt_execute($stmt);

    $resultData = mysqli_stmt_get_result($stmt);

    if ($row = mysqli_fetch_assoc($resultData)) {
        mysqli_stmt_close($stmt);
        return $row;
    } else {
        $result = false;
        mysqli_stmt_close($stmt);
        return $result;
    }
}

function updateSkillsParameters($conn, $skills, $studentId, $classKey) {
    $sql = "UPDATE skillsets
            SET skill1_value = ?, skill2_value = ?, skill3_value = ?, skill4_value =?, skill5_value =?
            WHERE student_id = ?;";
    $stmt = mysqli_stmt_init($conn);
    if (!mysqli_stmt_prepare($stmt, $sql)) {
        header("location: ../welcome.php?t=student&error=stmtfail");
        exit();
    } 
    mysqli_stmt_bind_param($stmt, "ssssss", $skills[0], $skills[1], $skills[2], $skills[3], $skills[4], $studentId);
    mysqli_stmt_execute($stmt);

    mysqli_stmt_close($stmt);
    header("location: ../student.php?class=".$classKey);
}

function getClassId($conn, $classKey) {
    $sql = "SELECT class_id FROM classes WHERE class_key = ?;";
    $stmt = mysqli_stmt_init($conn);
    if (!mysqli_stmt_prepare($stmt, $sql)) {
        header("location: ../welcome.php?t=student&error=stmtfail");
        exit();
    }
    mysqli_stmt_bind_param($stmt, "s", $classKey);
    mysqli_stmt_execute($stmt);

    $resultData = mysqli_stmt_get_result($stmt);
    $row = mysqli_fetch_assoc($resultData);
    
    mysqli_stmt_close($stmt);
    
    return $row['class_id'];

}

function getStudentList($conn) {
    $classKey = $_GET['class'];
    $sql = "SELECT user_first, user_last, 
    skill1_value, skill2_value, skill3_value, skill4_value, skill5_value,
    skill1_name, skill2_name, skill3_name, skill4_name, skill5_name
    FROM users, classes, class_registration, skillsets
    WHERE class_registration.class_code = classes.class_code 
    AND class_registration.student_id = users.user_id 
    AND skillsets.student_id = users.user_id
    AND class_key = ?;";
    
    $stmt = mysqli_stmt_init($conn);
    if (!mysqli_stmt_prepare($stmt, $sql)) {
        header("location: ../signup.php?error=stmtfail");
        exit();
    }
    mysqli_stmt_bind_param($stmt, "s", $classKey);
    mysqli_stmt_execute($stmt);

    $result = mysqli_stmt_get_result($stmt);
    $resultCheck = mysqli_num_rows($result);

    if ($resultCheck == mysqli_num_rows($result)) {
        if ($resultCheck != 0){
            $count = 0;
            while ($row = mysqli_fetch_assoc($result)) {
                if ($count == 0){
                    echo "<tr style='font-weight: bold;'>";
                    echo "<td>N.</td>";
                    echo "<td>Nom</td>";
                    echo "<td class='skill-name' style='color: red'>".ucfirst($row['skill1_name'])."</td>";
                    echo "<td class='skill-name' style='color: blue'>".ucfirst($row['skill2_name'])."</td>";
                    echo "<td class='skill-name' style='color: green'>".ucfirst($row['skill3_name'])."</td>";
                    echo "<td class='skill-name' style='color: orange'>".ucfirst($row['skill4_name'])."</td>";
                    echo "<td class='skill-name' style='color: purple'>".ucfirst($row['skill5_name'])."</td>";
                    echo "</tr>";
                }
                $count++;
                echo "<tr class='student'>";
                echo "<td>".$count."</td>";
                echo "<td class='student-name'>".$row['user_first']." ".$row['user_last']."</td>";
                echo "<td class='skill1' style='color: red'>".$row['skill1_value']."</td>";
                echo "<td class='skill2' style='color: blue'>".$row['skill2_value']."</td>";
                echo "<td class='skill3' style='color: green'>".$row['skill3_value']."</td>";
                echo "<td class='skill4' style='color: orange'>".$row['skill4_value']."</td>";
                echo "<td class='skill5' style='color: purple'>".$row['skill5_value']."</td>";
                echo "</tr>";
            }
            mysqli_stmt_close($stmt);
            return $count;
        } else {
            echo "<div style='padding: 10px 0; text-align: center;'>Aucun étudiant n'est inscrit à votre classe pour le moment.</div>";
            echo "<div style='text-align: center;'>Si vous ne l'avez pas encore fait, veuillez leur partager le code ".getClassCode($conn)." et attendre leur inscription.</div>";
            return 0;
        }
    } 
}

function getClassCode($conn) {
    $classKey = $_GET['class'];
    $sql = "SELECT class_code 
            FROM classes
            WHERE class_key = ?;";
    
    $stmt = mysqli_stmt_init($conn);
    if (!mysqli_stmt_prepare($stmt, $sql)) {
        header("location: ../signup.php?error=stmtfail");
        exit();
    }
    mysqli_stmt_bind_param($stmt, "s", $classKey);
    mysqli_stmt_execute($stmt);

    $result = mysqli_stmt_get_result($stmt);

    if ($row = mysqli_fetch_assoc($result)) {
        return $row['class_code'];
    }
}

function subjectExists($conn) {
    $sql = "SELECT * 
    FROM subject_groups, classes
    WHERE classes.class_id = subject_groups.class_id
    AND class_key = ?;";

    $stmt = mysqli_stmt_init($conn);
    if (!mysqli_stmt_prepare($stmt, $sql)) {
        header("location: ../signup.php?error=stmtfail");
        exit();
    }
    mysqli_stmt_bind_param($stmt, "s", $_GET['class']);
    mysqli_stmt_execute($stmt);
    $result = mysqli_stmt_get_result($stmt);;

    $resultCheck = mysqli_num_rows($result);
    if ($resultCheck == mysqli_num_rows($result)) {
        if($row = mysqli_fetch_assoc($result)){
            return true;
        } else {
            return false;
        }
    }
}

function createSubjectGroup($conn, $subjectGroupName, $classId, $classKey) {
    $sql = "INSERT INTO subject_groups (subject_group_name, class_id) 
            VALUES (?, ?);";
    $stmt = mysqli_stmt_init($conn);
    if (!mysqli_stmt_prepare($stmt, $sql)) {
        header("location: ../subject.php?t=".$_GET['class']."&error=stmtfail");
        exit();
    }

    mysqli_stmt_bind_param($stmt, "ss", $subjectGroupName, $classId);
    mysqli_stmt_execute($stmt);
    mysqli_stmt_close($stmt);
    header("location: ../subject.php?class=".$classKey);
    exit();
}

function getSubjectGroups($conn, $classKey) {
    $sql = "SELECT subject_group_id, subject_group_name, subject_group_enable, class_key
            FROM subject_groups, classes
            WHERE subject_groups.class_id = classes.class_id
            AND class_key = ?;";
    
    $stmt = mysqli_stmt_init($conn);
    if (!mysqli_stmt_prepare($stmt, $sql)) {
        header("location: ../signup.php?error=stmtfail");
        exit();
    }
    mysqli_stmt_bind_param($stmt, "s", $classKey);
    mysqli_stmt_execute($stmt);

    $result = mysqli_stmt_get_result($stmt);
    $resultCheck = mysqli_num_rows($result);

    if ($resultCheck == mysqli_num_rows($result)) {
        $count = 0;
        echo "<tr>";
        echo "<th>N.</th>";
        echo "<th>Name</th>";
        echo "<th>State</th>";
        echo "</tr>";
        while ($row = mysqli_fetch_assoc($result)) {
            if ($row['subject_group_enable'] == true){
                $text_btn = "Enabled";
            } else if ($row['subject_group_enable'] != true){
                $text_btn = "Disabled";
            }
            $count++;
            echo "<tr>";
            echo "<td class='classlink'>".$count."</td>";
            echo "<td class='classlink'><a href='subject-creation.php?id=".$row['subject_group_id']."&class=".$row['class_key']."'>".$row['subject_group_name']."</a></td>";
            echo '<td>';
            echo '<form action="includes/enablesubject.inc.php?id='.$row['subject_group_id'].'&class='.$_GET['class'].'" method="POST">';
            echo '<input type="hidden" name="enable" value="'.$row['subject_group_id'].'">';
            echo '<input type="submit" name="submit" value="'.$text_btn.'">';
            echo '</form>';
            echo '</td>';
            echo "</tr>";
        }
        mysqli_stmt_close($stmt);
    }
}

function cutString($str) {
    $words = explode(' ', $str);
    return $words;
}

function checkSubject($conn, $subjectGroup, $counter) {
    $sql = "SELECT * FROM subjects
            WHERE subject_group = ? AND subject_counter = ?;";

    $stmt = mysqli_stmt_init($conn);
    if (!mysqli_stmt_prepare($stmt, $sql)) {
        header("location: ../signup.php?t=".$_GET['t']."&error=stmtfail");
        exit();
    }

    mysqli_stmt_bind_param($stmt, "ss", $subjectGroup, $counter);
    mysqli_stmt_execute($stmt);

    $resultData = mysqli_stmt_get_result($stmt);
    
    if ($row = mysqli_fetch_assoc($resultData)) {
        mysqli_stmt_close($stmt);
        return true;
    } else {
        mysqli_stmt_close($stmt);
        return false;
    }
}

function createSubject($conn, $subjectName, $subjectTags, $subjectGroup, $counter, $classKey) {
    $sql = "INSERT INTO subjects (subject_name, subject_tags, subject_group, subject_counter) 
            VALUES (?, ?, ?, ?);";
    $stmt = mysqli_stmt_init($conn);
    if (!mysqli_stmt_prepare($stmt, $sql)) {
        header("location: ../subject.php?t=".$_GET['class']."&error=stmtfail");
        exit();
    }

    mysqli_stmt_bind_param($stmt, "ssss", $subjectName, $subjectTags, $subjectGroup, $counter);
    mysqli_stmt_execute($stmt);
    mysqli_stmt_close($stmt);
    header("location: ../subject-creation.php?id=".$subjectGroup."&class=".$classKey);
}

function updateSubject($conn, $subjectName, $subjectTags, $subjectGroup, $counter, $classKey) {
    $sql = "UPDATE subjects
            SET subject_name = ?, subject_tags = ?
            WHERE subject_group = ? AND subject_counter = ?;";
    $stmt = mysqli_stmt_init($conn);
    if (!mysqli_stmt_prepare($stmt, $sql)) {
        header("location: ../welcome.php?t=student&error=stmtfail");
        exit();
    } 
    mysqli_stmt_bind_param($stmt, "ssss", $subjectName, $subjectTags, $subjectGroup, $counter);
    mysqli_stmt_execute($stmt);

    mysqli_stmt_close($stmt);
    header("location: ../subject-creation.php?id=".$subjectGroup."&class=".$classKey);
}

function getStudentsNames($conn, $classKey) {
    $sql = "SELECT user_first, user_last 
            FROM users, classes, class_registration 
            WHERE class_registration.class_code = classes.class_code 
            AND user_id = student_id 
            AND class_key = ?;";

    $stmt = mysqli_stmt_init($conn);
    if (!mysqli_stmt_prepare($stmt, $sql)) {
        header("location: ../signup.php?error=stmtfail");
        exit();
    }
    mysqli_stmt_bind_param($stmt, "s", $classKey);
    mysqli_stmt_execute($stmt);

    $result = mysqli_stmt_get_result($stmt);
    $resultCheck = mysqli_num_rows($result);

    if ($resultCheck == mysqli_num_rows($result)) {
        $arr = array();
        while ($row = mysqli_fetch_assoc($result)) {
            array_push($arr, $row['user_first']." ".$row['user_last']);
        }
        mysqli_stmt_close($stmt);
        return implode(', ', $arr);
    }
}

function getStudentsSkills($conn, $classKey) {
    $sql = "SELECT skill1_value, skill2_value, skill3_value, skill4_value, skill5_value
            FROM skillsets, classes, class_registration
            WHERE class_registration.student_id = skillsets.student_id
            AND classes.class_code = class_registration.class_code
            AND class_key = ?;";

    $stmt = mysqli_stmt_init($conn);
    if (!mysqli_stmt_prepare($stmt, $sql)) {
        header("location: ../signup.php?error=stmtfail");
        exit();
    }
    mysqli_stmt_bind_param($stmt, "s", $classKey);
    mysqli_stmt_execute($stmt);

    $result = mysqli_stmt_get_result($stmt);
    $resultCheck = mysqli_num_rows($result);

    if ($resultCheck == mysqli_num_rows($result)) {
        $arr = array();
        while ($row = mysqli_fetch_assoc($result)) {
            array_push($arr, $row['skill1_value']." ".$row['skill2_value']." ".$row['skill3_value']." ".$row['skill4_value']." ".$row['skill5_value']);
        }
        mysqli_stmt_close($stmt);
        return implode(', ', $arr);
    }
}

function getSubjects($conn, $subjectGroupId){
    $sql = "SELECT subject_name, subject_tags
            FROM subjects
            WHERE subject_group = ?;";
    
    $stmt = mysqli_stmt_init($conn);
    if (!mysqli_stmt_prepare($stmt, $sql)) {
        header("location: ../signup.php?error=stmtfail");
        exit();
    }
    mysqli_stmt_bind_param($stmt, "s", $subjectGroupId);
    mysqli_stmt_execute($stmt);

    $result = mysqli_stmt_get_result($stmt);
    $resultCheck = mysqli_num_rows($result);

    if ($resultCheck == mysqli_num_rows($result)) {
        $count = 0;
        echo "<tr>";
            echo "<th class='classlink'>N.</th>";
            echo "<th class='classlink'>Subject Name</th>";
            echo "<th class='classlink'>Tags</th>";
        echo "</tr>";
        while ($row = mysqli_fetch_assoc($result)) {
            $tags = explode(" ", $row['subject_tags']);
            $count++;
            echo "<tr>";
                echo "<td class='classlink'>".$count."</td>";
                echo "<td class='classlink'>".$row['subject_name']."</td>";
                for ($i = 0; $i < 5; $i++){
                    if (isset($tags[$i])) {
                        echo "<td class='classlink'>".$tags[$i]."</td>";
                    } else {
                        echo "<td style='font-style: italic; color: grey;'>none</td>";
                    }
                }
                echo '
                <td>
                    <form action="includes/deletesubject.inc.php?id='.$_GET['id'].'&class='.$_GET['class'].'" method="POST">
                        <input type="hidden" name="delete" value="'.$count.'">
                        <input type="submit" name="submit" value="delete">
                    </form>
                </td>';
            echo "</tr>";
        }
        mysqli_stmt_close($stmt);
    }
}

function generateSubjectForm($conn, $subjectGroupId) {
    $sql = "SELECT subject_name, subject_tags FROM subjects
            WHERE subject_group = ?";

    $stmt = mysqli_stmt_init($conn);
    if (!mysqli_stmt_prepare($stmt, $sql)) {
        header("location: ../signup.php?t=".$_GET['t']."&error=stmtfail");
        exit();
    }

    mysqli_stmt_bind_param($stmt, "s", $subjectGroupId);
    mysqli_stmt_execute($stmt);

    $result = mysqli_stmt_get_result($stmt);
    $resultCheck = mysqli_num_rows($result);

    if ($resultCheck == mysqli_num_rows($result)) {
        $count = 0;       
        if ($resultCheck == 0){
            $count++;
            echo "<p>Sujet 1</p>";
            echo '<input class="input-field" type="text" name="subject1 value="" placeholder="Nom du sujet...">';
            echo '<input class="input-field" type="text" name="tags1" value="" placeholder="Nom du sujet...">';
        } else {
            while ($row = mysqli_fetch_assoc($result)) {
                $count++;
                echo "<p>Sujet ".$count."</p>";
                echo '<input class="input-field" type="text" name="subject'.$count.'" value="'.$row['subject_name'].'" placeholder="Nom du sujet...">';
                echo '<input class="input-field" type="text" name="tags'.$count.'" value="'.$row['subject_tags'].'" placeholder="Nom du sujet...">';
            }
        }

        $_POST['n_subjects'] = $count;
        mysqli_stmt_close($stmt);
    }
}

function deleteSubject($conn, $group, $subject){
    $sql = "DELETE FROM subjects 
            WHERE subject_group = ?
            AND subject_counter = ?;";

    $stmt = mysqli_stmt_init($conn);
        if (!mysqli_stmt_prepare($stmt, $sql)) {
            header("location: ../signup.php?t=".$_GET['t']."&error=stmtfail");
            exit();
        }

    mysqli_stmt_bind_param($stmt, "ss", $group, $subject);
    mysqli_stmt_execute($stmt);

    mysqli_stmt_close($stmt);
    $id_set = getSubjectId($conn, $group);
    for ($i = 0; $i < sizeof($id_set); $i++){
        echo $i;
        echo "<br>";
        $id = $id_set[$i]['subject_id'];
        updateSubjectCounter($conn, ($i+1)*2, $id, $group);
    }

    print_r($id_set[0]['subject_id']);
    header("location: ../subject-creation.php?id=".$_GET['id']."&class=".$_GET['class']);   
}

function getSubjectId($conn, $group){
    $sql = "SELECT subject_id
            FROM subjects
            WHERE subject_group = ?;";

    $stmt = mysqli_stmt_init($conn);
    if (!mysqli_stmt_prepare($stmt, $sql)) {
    header("location: ../signup.php?error=stmtfail");
    exit();
    }
    mysqli_stmt_bind_param($stmt, "s", $group);
    mysqli_stmt_execute($stmt);

    $result = mysqli_stmt_get_result($stmt);
    mysqli_stmt_close($stmt);
    return $result->fetch_all(MYSQLI_ASSOC); 
}
function updateSubjectCounter($conn, $counter, $id, $group){
    $sql = "UPDATE subjects
            SET subject_counter = ?
            WHERE subject_id = ? 
            AND subject_group = ?;";
    
    $stmt = mysqli_stmt_init($conn);
        if (!mysqli_stmt_prepare($stmt, $sql)) {
            header("location: ../signup.php?t=".$_GET['t']."&error=stmtfail");
            exit();
        }

    mysqli_stmt_bind_param($stmt, "sss", $counter, $id, $group);
    mysqli_stmt_execute($stmt);

    mysqli_stmt_close($stmt);
}

function enableSubject($conn, $id){
    $sql = "UPDATE subject_groups
    SET subject_group_enable = true
    WHERE subject_group_id = ?;";

    $stmt = mysqli_stmt_init($conn);
    if (!mysqli_stmt_prepare($stmt, $sql)) {
        header("location: ../subject.php?t=".$_GET['t']."&error=stmtfail1");
        exit();
    }

    mysqli_stmt_bind_param($stmt, "s", $id);
    mysqli_stmt_execute($stmt);

    mysqli_stmt_close($stmt);
    header("location: ../subject.php?class=".$_GET['class']);
}

function resetEnabled($conn, $class_key){
    $sql = "UPDATE subject_groups
    SET subject_group_enable = false
    WHERE class_id = (SELECT classes.class_id
                      FROM classes
                      WHERE classes.class_id = subject_groups.class_id
                      AND subject_group_enable = true
                      AND class_key = ?);";
        $stmt = mysqli_stmt_init($conn);
        if (!mysqli_stmt_prepare($stmt, $sql)) {
            header("location: ../subject.php?class=".$class_key."&t=".$_GET['t']."&error=stmtfail2");
            exit();
        }
    
        mysqli_stmt_bind_param($stmt, "s", $class_key);
        mysqli_stmt_execute($stmt);
    
        mysqli_stmt_close($stmt);
        header("location: ../subject.php?class=".$class_key);
}

function getSubjectName($conn) {
    $classKey = $_GET['class'];
    $sql = "SELECT subject_group_name
            FROM subject_groups, classes
            WHERE subject_group_enable = true
            AND subject_groups.class_id = classes.class_id
            AND class_key = ?;";
    
    $stmt = mysqli_stmt_init($conn);
    if (!mysqli_stmt_prepare($stmt, $sql)) {
        header("location: ../signup.php?error=stmtfail");
        exit();
    }
    mysqli_stmt_bind_param($stmt, "s", $classKey);
    mysqli_stmt_execute($stmt);

    $result = mysqli_stmt_get_result($stmt);
    mysqli_stmt_close($stmt);
    if ($row = mysqli_fetch_assoc($result)) {
        return $row['subject_group_name'];
    }
    
}

function getTags($conn){
    $classKey = $_GET['class'];
    $sql = "SELECT subject_tags
            FROM subjects, subject_groups, classes
            WHERE subject_group_enable = true
            AND subject_group_id = subject_group
            AND subject_groups.class_id = classes.class_id
            AND class_key = ?;";
    
    $stmt = mysqli_stmt_init($conn);
    if (!mysqli_stmt_prepare($stmt, $sql)) {
        header("location: ../signup.php?error=stmtfail");
        exit();
    }
    mysqli_stmt_bind_param($stmt, "s", $classKey);
    mysqli_stmt_execute($stmt);

    $result = mysqli_stmt_get_result($stmt);
    mysqli_stmt_close($stmt);
    return $result->fetch_all(MYSQLI_ASSOC);
}

function flatten(array $demo_array) {
    $new_array = array();
    array_walk_recursive($demo_array, function($array) use (&$new_array) { $new_array[] = $array; });
    return $new_array;
}

function checkTags($conn, $studentId){
    $sql = "SELECT tag_list 
            FROM tags, subject_groups
            WHERE tag_subject_group = subject_group_id
            AND student_id = ?;";

    $stmt = mysqli_stmt_init($conn);
    if (!mysqli_stmt_prepare($stmt, $sql)) {
        header("location: ../signup.php?t=".$_GET['t']."&error=stmtfail");
        exit();
    }

    mysqli_stmt_bind_param($stmt, "s", $studentId);
    mysqli_stmt_execute($stmt);

    $resultData = mysqli_stmt_get_result($stmt);
    
    if ($row = mysqli_fetch_assoc($resultData)) {
        mysqli_stmt_close($stmt);
        return true;
    } else {
        mysqli_stmt_close($stmt);
        return false;
    }
}

function sendTags($conn, $tags, $studentId, $group) {
    $sql = "INSERT INTO tags (tag_list, student_id, tag_subject_group) 
    VALUES (?, ?, ?);";
    $stmt = mysqli_stmt_init($conn);
    if (!mysqli_stmt_prepare($stmt, $sql)) {
        header("location: ../welcome.php?t=student&error=stmtfail");
        exit();
    } 
    mysqli_stmt_bind_param($stmt, "sss", $tags, $studentId, $group);
    mysqli_stmt_execute($stmt);

    mysqli_stmt_close($stmt);
    header("location: ../student.php?class=".$_GET['class']);
}

function updateTAgs($conn, $tags, $studentId) {
    $sql = "UPDATE tags
            SET tag_list = ?
            WHERE student_id = ?;";
    $stmt = mysqli_stmt_init($conn);
    if (!mysqli_stmt_prepare($stmt, $sql)) {
        header("location: ../welcome.php?t=student&error=stmtfail");
        exit();
    } 
    mysqli_stmt_bind_param($stmt, "ss", $tags, $studentId);
    mysqli_stmt_execute($stmt);

    mysqli_stmt_close($stmt);
    header("location: ../student.php?class=".$_GET['class']);
}

function getSubjectGroup($conn){
    $classKey = $_GET['class'];
    $sql = "SELECT subject_group_id
            FROM subject_groups, classes
            WHERE subject_group_enable = true
            AND subject_groups.class_id = classes.class_id
            AND class_key = ?";
    
    $stmt = mysqli_stmt_init($conn);
    if (!mysqli_stmt_prepare($stmt, $sql)) {
        header("location: ../signup.php?error=stmtfail");
        exit();
    }
    mysqli_stmt_bind_param($stmt, "s", $classKey);
    mysqli_stmt_execute($stmt);

    $result = mysqli_stmt_get_result($stmt);

    if ($row = mysqli_fetch_assoc($result)) {
        mysqli_stmt_close($stmt);
        return $row['subject_group_id'];
    }


}

function getSubjectNames($conn, $group) {
    $sql = "SELECT subject_name
            FROM subjects, subject_groups
            WHERE subject_group = subject_group_id
            AND subject_group_name = ?;";
    
    $stmt = mysqli_stmt_init($conn);
    if (!mysqli_stmt_prepare($stmt, $sql)) {
        header("location: ../signup.php?error=stmtfail");
        exit();
    }
    mysqli_stmt_bind_param($stmt, "s", $group);
    mysqli_stmt_execute($stmt);

    $result = mysqli_stmt_get_result($stmt);
    
    mysqli_stmt_close($stmt);
    return $result->fetch_all(MYSQLI_ASSOC); 

    
}

function getSubjectTags($conn, $group) {
    $sql = "SELECT subject_tags
            FROM subjects, subject_groups
            WHERE subject_group = subject_group_id
            AND subject_group_name = ?;";
    
    $stmt = mysqli_stmt_init($conn);
    if (!mysqli_stmt_prepare($stmt, $sql)) {
        header("location: ../signup.php?error=stmtfail");
        exit();
    }
    mysqli_stmt_bind_param($stmt, "s", $group);
    mysqli_stmt_execute($stmt);

    $result = mysqli_stmt_get_result($stmt);
    
    mysqli_stmt_close($stmt);
    return $result->fetch_all(MYSQLI_ASSOC);
}

function getStudentsTags($conn, $classKey) {
    $sql = "SELECT tag_list
            FROM tags, classes, class_registration
            WHERE class_registration.student_id = tags.student_id
            AND classes.class_code = class_registration.class_code
            AND class_key = ?;";

    $stmt = mysqli_stmt_init($conn);
    if (!mysqli_stmt_prepare($stmt, $sql)) {
        header("location: ../signup.php?error=stmtfail");
        exit();
    }
    mysqli_stmt_bind_param($stmt, "s", $classKey);
    mysqli_stmt_execute($stmt);

    $result = mysqli_stmt_get_result($stmt);
    $resultCheck = mysqli_num_rows($result);
    if ($resultCheck == mysqli_num_rows($result)) {  
        mysqli_stmt_close($stmt);
        return $result->fetch_all(MYSQLI_ASSOC);
    }
}

function validateGroups($conn, $groupName, $groupMembers, $classKey) {
    $sql = "INSERT INTO groupes (group_name, group_members, class_key) 
    VALUES (?, ?, ?);";
    $stmt = mysqli_stmt_init($conn);
    if (!mysqli_stmt_prepare($stmt, $sql)) {
        header("location: ../welcome.php?t=student&error=stmtfail");
        exit();
    } 
    mysqli_stmt_bind_param($stmt, "sss", $groupName, $groupMembers, $classKey);
    mysqli_stmt_execute($stmt);

    mysqli_stmt_close($stmt);
}

function checkGroups($conn, $classKey) {
    $sql = "SELECT class_key FROM groupes";
    $result = mysqli_query($conn, $sql);

    while ($row = mysqli_fetch_assoc($result)) {
        if ($row['class_key'] == $classKey) {
            $keyExists = true;
            break;
        } else {
            $keyExists = false;
        }
    }
    return $keyExists;
}

function deleteGroups($conn, $classKey){
    $sql = "DELETE FROM groupes
            WHERE class_key = ?;";

    $stmt = mysqli_stmt_init($conn);
        if (!mysqli_stmt_prepare($stmt, $sql)) {
            header("location: ../signup.php?t=".$_GET['t']."&error=stmtfail");
            exit();
        }

    mysqli_stmt_bind_param($stmt, "s", $classKey);
    mysqli_stmt_execute($stmt);

    mysqli_stmt_close($stmt);
}

function getFullName($conn, $userId) {
    $sql = "SELECT user_first, user_last 
            FROM users
            WHERE user_id = ?;";

    $stmt = mysqli_stmt_init($conn);
    if (!mysqli_stmt_prepare($stmt, $sql)) {
        header("location: ../signup.php?error=stmtfail");
        exit();
    }
    mysqli_stmt_bind_param($stmt, "s", $userId);
    mysqli_stmt_execute($stmt);

    $result = mysqli_stmt_get_result($stmt);
    $resultCheck = mysqli_num_rows($result);

    if ($resultCheck == mysqli_num_rows($result)) {
        if ($row = mysqli_fetch_assoc($result)) {
            mysqli_stmt_close($stmt);
            return $row['user_first']." ".$row['user_last'];
        }
    }
}

function getGroup($conn, $fullName) {
    $fullName = "%$fullName%";
    $sql = "SELECT group_name, group_members
    FROM groupes
    WHERE group_members LIKE ?;";

    $stmt = mysqli_stmt_init($conn);
    if (!mysqli_stmt_prepare($stmt, $sql)) {
        header("location: ../signup.php?error=stmtfail");
        exit();
    }
    mysqli_stmt_bind_param($stmt, "s", $fullName);
    mysqli_stmt_execute($stmt);

    $result = mysqli_stmt_get_result($stmt);
    $resultCheck = mysqli_num_rows($result);

    if ($resultCheck == mysqli_num_rows($result)) {
        if ($row = mysqli_fetch_assoc($result)) {
            mysqli_stmt_close($stmt);
            return $row;
        }
    }
}