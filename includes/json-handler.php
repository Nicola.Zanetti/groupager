<?php

require_once 'dbh.inc.php';
require_once 'functions.inc.php';

$requestPayload = file_get_contents("php://input");
$object = json_decode($requestPayload, true);

var_dump($object);

$classKey = $object[0]['class'];

if(checkGroups($conn, $classKey)){
    deleteGroups($conn, $classKey);
}

for ($i = 0; $i < count($object); $i++){
    $groupName = $object[$i]['name'];
    $groupMembers = implode(", ", $object[$i]['members']);
    validateGroups($conn, $groupName, $groupMembers, $classKey);
}
