<?php

if (isset($_GET['t'], $_POST['submit'])) {
    $email = $_POST["email"];
    $pwd = $_POST["pwd"];
    $status = $_GET['t'];

    require_once 'dbh.inc.php';
    require_once 'functions.inc.php';

    if (emptyInputLogin($email, $pwd) !== false) {
        header("location: ../login.php?t=".$_GET['t']."&error=empty");
        exit();
    }

    loginUser($conn, $email, $pwd, $status);
}
else {
    header("location: ../login.php");
    exit();
}