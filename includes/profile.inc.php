<?php

session_start();
require_once 'dbh.inc.php';
require_once 'functions.inc.php';

if (isset($_GET['class'], $_POST['submit'])) {
    echo 'if ok';
    $classKey = $_GET['class'];
    $studentId = $_SESSION['user_id'];
    $classId = getClassId($conn, $classKey);
    $skillNames = array(
        "writing", 
        "it",
        "communication",
        "visual",
        "relations");
    $skills = array(
        $_POST["writing"], 
        $_POST["it"],
        $_POST["communication"],
        $_POST["visual"],
        $relations = $_POST["relations"]);

    $skillExists = checkParameters($conn, $studentId);
    if ($skillExists == false) {
        sendSkillsParameters($conn, $studentId, $skillNames, $skills, $classKey);
    } else {
        updateSkillsParameters($conn, $skills, $studentId, $classKey);
    }
    
} else {
    header("location: ../welcom.php");
    exit();
}