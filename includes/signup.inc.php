<?php

if (isset($_GET['t'], $_POST['submit'])) {
    $first = $_POST['first'];
    $last = $_POST['last'];
    $email = $_POST['email'];
    $pwd = $_POST['pwd'];
    $pwdRepeat = $_POST['pwdrepeat'];
    $status = $_GET['t'];

    require_once 'dbh.inc.php';
    require_once 'functions.inc.php';

    if (emptyInputSignup($first, $last, $email, $pwd, $pwdRepeat) !== false) {
        header("location: ../signup.php?t=".$_GET['t']."&error=empty");
        exit();
    }
    if (invalidName($first) !== false) {
        header("location: ../signup.php?t=".$_GET['t']."&error=invalidname");
        exit();
    }
    if (invalidSurname($last) !== false) {
        header("location: ../signup.php?t=".$_GET['t']."&error=invalidsurname");
        exit();
    }
    if (invalidEmail($email) !== false) {
        header("location: ../signup.php?t=".$_GET['t']."&error=invalidemail");
        exit();
    }
    if (pwdMatch($pwd, $pwdRepeat) !== false) {
        header("location: ../signup.php?t=".$_GET['t']."&error=pwderror");
        exit();
    }
    if (emailExists($conn, $email, $status) !== false) {
        header("location: ../signup.php?t=".$_GET['t']."&error=emailexists");
        exit();
    }

    createUser($conn, $first, $last, $email, $pwd, $status);

} else {
    header("location: ../signup.php?t=".$_GET['t']);
    exit();
}