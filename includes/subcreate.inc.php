<?php

session_start();

if (isset($_POST['submit'])) {

    require_once 'dbh.inc.php';
    require_once 'functions.inc.php';
    
    $classKey = $_GET['class'];
    $subjectGroup = $_GET['id'];
    $count = 0;
    $tags = '';

    foreach ($_POST as $key => $value){
        $count++;
        if ($key != "submit"){
            if ($count % 2 == 1){
                $subjectName = $value;
            }
            if ($count % 2 == 0){
                $tags = $value;
                $check = checkSubject($conn, $subjectGroup, $count);
                if ($check === false){
                    createSubject($conn, $subjectName, $tags, $subjectGroup, $count, $classKey);
                } else if ($check === true) {
                    updateSubject($conn, $subjectName, $tags, $subjectGroup, $count, $classKey);
                } else {
                    echo "Something went wrong";
                }
            } 
        }
    }

    if (emptyInputClass($subjectName) !== false) {
        header("location: ../subject-creation.php?id=".$_GET['id']."&class=".$_GET['class']."&error=empty");
        exit();
    }
    if (sizeof(cutString($value)) > 5) {
        header("location: ../subject-creation.php?id=".$_GET['id']."&class=".$_GET['class']."&error=tags");
        exit();
    }


} else {
    header("location: ../login.php");
    exit();
}