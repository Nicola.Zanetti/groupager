<?php

session_start();

if (isset($_POST['submit'])) {

    require_once 'dbh.inc.php';
    require_once 'functions.inc.php';
    
    $classKey = $_GET['class'];
    $subjectGroupName = $_POST['subject-group'];
    $classId = getClassId($conn, $classKey);
    
    if (emptyInputClass($subjectGroupName) !== false) {
        header("location: ../subject.php?class=".$_GET['class']."&error=empty");
        exit();
    }
    createSubjectGroup($conn, $subjectGroupName, $classId, $classKey);  

} else {
    header("location: ../login.php");
    exit();
}