<?php

session_start();
require_once 'dbh.inc.php';
require_once 'functions.inc.php';

if (isset($_GET['class'], $_POST['submit'])) {
    $studentId = $_SESSION['user_id'];
    $classId = getClassId($conn, $_GET['class']);
    $group = getSubjectGroup($conn);
    array_pop($_POST);
    $tags = implode(" ", array_keys($_POST));
    $tagExists = checkTags($conn, $studentId);
    if ($tagExists == false) {
        sendTags($conn, $tags, $studentId, $group);
    } else {
        updateTags($conn, $tags, $studentId, $group);
    }
} else {
    header("location: ../welcome.php");
    exit();
}

