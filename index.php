<?php
    session_start();
    if (isset($_SESSION['user_status'])) {
        header("location: welcome.php?t=".$_SESSION['user_status']);
    }
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Groupager</title>
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Josefin+Sans&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="assets/css/reset.css"/>
    <link rel="stylesheet" href="assets/css/style.css"/>
    <link rel="stylesheet" href="assets/css/home.css"/>
    <script src="assets/js/app.js"></script>
</head>
<body>
    <main>
        <div class="home-title">GROUPAGER</div>
        <div class="home-content">
            <div class="left">
                <a class="home-btn stdnt-btn" href="login.php?t=student">Espace Étudiant</a>
            </div>
            <div class="right">
                <a class="home-btn tchr-btn" href="login.php?t=teacher">Espace Professeur</a>               
            </div>
        </div>
    </main>
</body>
</html>