<?php
    include_once 'header.php'
?>

<main>
    <div class="form-container">
        <form  class="form" action="includes/login.inc.php?t=<?php echo $_GET['t']?>" method="POST">
            <h2 class="form-title">Login <?php 
            
            if (isset($_GET['t'])) {
                if ($_GET['t'] == "student") {
                    echo "étudiant";
                } else if (($_GET['t'] == "teacher")) {
                    echo "enseignant"; 
                } else {
                    header("location: index.php");
                }
            }

            ?></h2>
            <input class="input-field" type="text" name="email" placeholder="E-mail...">
            <input class="input-field" type="password" name="pwd" placeholder="Mot de passe...">
            <button class="submit-btn" type="submit" name="submit">Login</button>
            <?php
                if (isset($_SESSION['user_status'])) {
                    header("location: welcome?t=".$_SESSION['user_status']);

                }
                if (!isset($_GET['t'])) {
                    header("location: index.php");
                    exit();
                }
                if (isset($_GET['error'])) {
                    if ($_GET['error'] == "empty") {
                        echo "<p class='error'>Vous devez remplir tous les champs</p>";
                    }
                    else if  ($_GET['error'] == 'wronglogin') {
                        echo "<p class='error'>Votre email ou mot de passe sont incorrectes.</p>";
                    }
                }
            ?>
            <a class="clickable-text" href="signup.php?t=<?php echo $_GET['t']?>">Vous n'avez pas encore de compte?</a>
        </form>
    </div>
</main>

<?php
    include_once 'footer.php'
?>

</body>
</html>