<?php
    include_once 'header.php'
?>

<main>
    <div class="form-container">
        <form  class="form" action="includes/signup.inc.php?t=<?php echo $_GET['t']?>" method="POST">
            <h2>Créer un compte</h2>
            <input class="input-field" type="text" name="first" placeholder="Prènom...">
            <input class="input-field" type="text" name="last" placeholder="Nom...">
            <input class="input-field" type="text" name="email" placeholder="E-mail...">
            <input class="input-field" type="password" name="pwd" placeholder="Mot de passe...">
            <input class="input-field" type="password" name="pwdrepeat" placeholder="Répéter mot de passe...">
            <button type="submit" name="submit">Sign Up</button>
            <?php
                if (isset($_SESSION['user_status'])) {
                    if ($_SESSION['user_status'] == "teacher") {
                        header("location: classroom.php");
                    } else if ($_SESSION['user_status'] == "student") {
                        header("location: student.php");
                    }
                }
                if (!isset($_GET['t'])) {
                    header("location: index.php");
                    exit();
                }
                if (isset($_GET['error'])) {
                    if ($_GET['error'] == "empty") {
                        echo "<p class='error'>Vous devez remplir tous les champs</p>";
                    }
                    else if ($_GET['error'] == "invalidname" || $_GET['error'] == "invalidsurname") {
                        echo "<p class='error'>Votre prénom ou nom conteinnent des charcteres invalides.</p>";
                    }
                    else if  ($_GET['error'] == 'invalidemail') {
                        echo "<p class='error'>Veuillez inserer un adresse email valable.</p>";
                    }
                    else if  ($_GET['error'] == 'pwderror') {
                        echo "<p class='error'>Les mots de passe ne sont pas les mêmes. Veuillez essayer de nouveau.</p>";
                    }
                    else if  ($_GET['error'] == 'emailexists') {
                        echo "<p class='error'>Il y a déjà un compte crée avec le même adresse email.</p>";
                    }
                    else if  ($_GET['error'] == 'stmtfailed') {
                        echo "<p class='error'>Il y un erreur inconnu, veuillez réessayer.</p>";
                    }
                }
            ?>
        </form>
    </div>
</main>

<?php
    include_once 'footer.php'
?>

</body>
</html>