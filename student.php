<?php
    include_once 'header.php';
    include_once 'includes/dbh.inc.php';
    include_once 'includes/functions.inc.php';
    $fullName = getFullName($conn, $_SESSION['user_id']);
    echo $fullName;
?>
    <main style="background-color: white;">
        <section class="title-field2">
        
            <?php 
                    echo "<h2 class='title'><span style='color: orange;'>".getClassName($conn)."</span></h2>";
                    echo "<a class='change' href='welcome.php?t=student'>Changer classe</a>";
            ?>
        </section >
        <section class="groups">
            <h2 class="title">Mon groupe</h2>
            <?php
                $groupName = getGroup($conn, $fullName)['group_name'];
                $groupMembers = getGroup($conn, $fullName)['group_members'];
                echo "<p>".$groupName."</p>";
                echo "<br>";
                echo "<p>".$groupMembers."</p>";
            ?>
        </section>
        <section class="profile">
            <h2 class="title">Mon profil</h2>
            <form action="includes/profile.inc.php?class=<?php echo $_GET['class'] ?>" method="POST">
                <label for="writing">Comment vous jugez vos compétences dans le domaine redaction?</label>
                <br>
                <select name="writing">
                    <option value="0">Très mauvaises</option>
                    <option value="25">Mauvaises</option>
                    <option value="50">Ni bonnes ni mausvaises</option>
                    <option value="75">Bonnes</option>
                    <option value="100">Très bonnes</option>
                </select>
                <br>
                <label for="it">Comment vous jugez vos compétences dans le domaine informatique?</label>
                <br>
                <select name="it">
                    <option value="0">Je n'ai jamais touché un ordinateur, c'est un miracle si j'arrive à l'allumer.</option>
                    <option value="25">J'utilise peu l'ordinateur, dés qu'il y a un problème j'ai besoin d'assistance.</option>
                    <option value="50">Je sais comment faire des tâches basiques comme rédiger un document, naviguer sur intérnet et installer des logiciels.</option>
                    <option value="75">Je sais résoudre pas mal de problèmes téchniques et j'ai confiance avec le système d'exploitation que j'utilise.</option>
                    <option value="100">L'ordinateur a peu de sécrets pour moi, je connais la programmation et je trouve une solution à tous les problèmes.</option>
                </select>
                <br>
                <label for="communication">Comment vous jugez vos compétences dans le domaine communication orale?</label>
                <br>
                <select name="communication">
                    <option value="0">Très mauvaises</option>
                    <option value="25">Mauvaises</option>
                    <option value="50">Ni bonnes ni mausvaises</option>
                    <option value="75">Bonnes</option>
                    <option value="100">Très bonnes</option>
                </select>
                <br>
                <label for="visual">Comment vous jugez vos compétences dans le domaine design et expression visuelle?</label>
                <br>
                <select name="visual">
                    <option value="0">Très mauvaises</option>
                    <option value="25">Mauvaises</option>
                    <option value="50">Ni bonnes ni mausvaises</option>
                    <option value="75">Bonnes</option>
                    <option value="100">Très bonnes</option>
                </select>
                <br>
                <label for="relations">Comment vous jugez vos compétences dans le domaine relations interpersonnelles?</label>
                <br>
                <select name="relations">
                    <option value="0">Très mauvaises</option>
                    <option value="25">Mauvaises</option>
                    <option value="50">Ni bonnes ni mausvaises</option>
                    <option value="75">Bonnes</option>
                    <option value="100">Très bonnes</option>
                </select>
                <br>
                <button type="submit" name="submit">Valider</button>
            </form>
        </section>
        <section class="subject">
            <h2 class="title">Compétences spécifiques</h2>
            <form action="includes/tags.inc.php?class=<?php echo $_GET['class'] ?>" method="POST">
                <?php
                    $a = array_unique(explode(" ", implode(" ",flatten(getTags($conn)))));
                    sort($a);
                    echo '<form action="includes/tags.inc.php?class='.$_GET['class'].'" method="POST">';
                    for($i = 0; $i < sizeof($a); $i++){
                        echo '
                            <input type="checkbox" name="'.$a[$i].'">
                            <label for="'.$a[$i].'">'.$a[$i].'</label>
                            <br><br>
                        ';
                    }
                    echo '<input type="submit" name="submit" value="Valider">';
                    echo '</form>';
                ?>
            </form>
        </section>
<!--         <section class="questionary field2">
            <h2 class="title">Questionnaires</h2>
        </section> -->
    </main>
<?php
    include_once 'footer.php'
?>
</body>
</html>