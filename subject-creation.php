<?php
    include_once 'header.php';
    include_once 'includes/dbh.inc.php';
    include_once 'includes/functions.inc.php';
?>

<main style="display: flex;">
    <div class="form-container">
        <form id="sub-form" class="form" action="includes/subcreate.inc.php?id=<?php echo $_GET['id']?>&class=<?php echo $_GET['class']?>" method="POST">
            <?php generateSubjectForm($conn, $_GET['id'])?>
            <button id="submit-btn" type="submit" name="submit">Valider</button>
            <button type="button" name="add" onclick="addForm()">Ajouter</button>
            <?php

                if (isset($_GET['error'])) {
                    if ($_GET['error'] == "empty") {
                        echo "<p class='error'>Vous ne pouvez pas laisser le champ vide</p>";
                    }
                    if ($_GET['error'] == "tags") {
                        echo "<p class='error'>Vous avez dépassé la limite de 5 compétences</p>";
                    }
                }
            ?>
        </form>
    </div>
    <div class="table-container">
        <table>
            <?php
                getSubjects($conn, $_GET['id']);
            ?>
        </table>
    </div>
</main>

<?php
    include_once 'footer.php';
?>

<script>
    var formCount = "<?php echo $_POST['n_subjects']; ?>";
    function addForm(){
        formCount++
        const subForm = document.getElementById("sub-form")
        const submitBtn = document.getElementById("submit-btn")
        const title = document.createElement("p")
        const subject = document.createElement("input")
        const tags = document.createElement("input")
        title.innerHTML = "Sujet " + formCount
        subject.className = "input-field"
        subject.setAttribute('type', 'text')
        subject.setAttribute('name', 'subject' + formCount)
        subject.setAttribute('placeholder', "Nom du sujet...")
        tags.className = "input-field"
        tags.setAttribute('type', 'text')
        tags.setAttribute('name', 'tags' + formCount)
        tags.setAttribute('placeholder', 'Listez max 5 compétences...')
        subForm.insertBefore(title, submitBtn)
        subForm.insertBefore(subject, submitBtn)
        subForm.insertBefore(tags, submitBtn)
    }
</script>
<script src="assets/js/common.js"></script>
</body>
</html>