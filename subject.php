<?php
    include_once 'header.php';
    include_once 'includes/dbh.inc.php';
    include_once 'includes/functions.inc.php';
?>

<main>
    <div class="form-container" style="flex-direction: column;">
        <form  class="form" action="includes/subjectcreate.inc.php?class=<?php echo $_GET['class']?>" method="POST">
            <h2>Veuillez créer votre groupe de sujets</h2>
            <input class="input-field" type="text" name="subject-group" placeholder="Nom du groupe de sujets...">
            <button type="submit" name="submit">Valider</button>
            <?php
                if (isset($_GET['error'])) {
                    if ($_GET['error'] == "empty") {
                        echo "<p class='error'>Vous ne pouvez pas laisser le champ vide</p>";
                    }
                    if ($_GET['error'] == "invalidcode") {
                        echo "<p class='error'>Le code que vous avez inseré n'est pas valide</p>";
                    }

                }
            ?>
        </form>
        <table>
            <?php
                getSubjectGroups($conn, $_GET['class']);
            ?>
        </table>
        <?php
        echo '<a class="btn-small" href="classroom.php?class='.$_GET['class'].'">Retour</a>';
        ?>
    </div>
</main>

<?php
    include_once 'footer.php'
?>

<script src="assets/js/common.js"></script>
</body>
</html>