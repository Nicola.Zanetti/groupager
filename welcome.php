<?php
    include_once 'header.php';
    include_once 'includes/dbh.inc.php';
    include_once 'includes/functions.inc.php';
?>

<main>
    <div class="form-container">
        <form  class="form" action="includes/classcreate.inc.php?t=<?php echo $_GET['t']?>" method="POST">
            <?php 
                if (isset($_GET['t'])) {
                    if ($_GET['t'] == "teacher") {
                        echo "<h2>Veuillez nommer votre classe</h2>";
                    } else if ($_GET['t'] == "student") {
                        echo "<h2>Veuillez saisir le code donné par votre enseignant</h2>";
                    } else {
                        header("location: index.php");
                    }
                } else {
                    header("location: index.php");
                }
            ?>
            
            <input class="input-field" type="text" name="class" placeholder="Nom de la classe...">
            <button type="submit" name="submit">Valider</button>
            <?php
                if (isset($_GET['error'])) {
                    if ($_GET['error'] == "empty") {
                        echo "<p class='error'>Vous ne pouvez pas laisser le champ vide</p>";
                    }
                    if ($_GET['error'] == "invalidcode") {
                        echo "<p class='error'>Le code que vous avez inseré n'est pas valide</p>";
                    }

                }
            ?>
            <table>
                <?php
                    if ($_GET['t'] == "teacher") {
                        getClassesTeacher($conn);
                    } else if ($_GET['t'] == "student") {
                        getClassesStudent($conn);
                    } else {
                        
                    }
                ?>
            </table>
        </form>
    </div>
</main>

<?php
    include_once 'footer.php'
?>

<script src="assets/js/common.js"></script>
</body>
</html>